
namespace BazaB.Models
{

    public class CreateProdavnicaDTO
    {
        public Prodavnica Prodavnica { get; set; }
        
        public int OkrugId { get; set; }
    }
}