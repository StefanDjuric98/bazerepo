using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Neo4j.Driver;
using System.Text;
using BazaB.Models;
using System.Net.Http;
using BazaB.Services;
using Microsoft.AspNetCore.Authorization;

[Route("api/[controller]")]
[ApiController]
public class ProdavacController : ControllerBase
{
    private readonly IDriver _driver;
    private readonly IAuthRepository _authRepository;
    private readonly RedisService _redisService;

    public ProdavacController(IDriver driver, IAuthRepository authRepository, RedisService redisService)
    {
        _driver = driver;
        _authRepository = authRepository;
        _redisService = redisService;
    }

    [Authorize(Roles = "Admin")]
    [HttpPost]
    [Route("KreirajProdavca")]
    public async Task<IActionResult> CreateProdavac(CreateProdavacDTO prodavacDTO)
    {
        var id = await _authRepository.Register(prodavacDTO.Prodavac, prodavacDTO.Password);
            if(id == null)
                return BadRequest("Node with the same username already exists in the database");

        await CreateLinkProdavnicaProdavac(prodavacDTO.ProdavnicaId, id.Value);
        return Ok("Node id: " + id.Value + " has been created in the database");
    }

    [HttpPost]
    [Route("Login")]
    public async Task<IActionResult> Login(LoginDTO loginInfo)
    {
        var token = await _authRepository.Login(loginInfo.username, loginInfo.password);
        if(token == null)
            return BadRequest("Wrong username or password");
        return Ok(token);
    }

    [Authorize(Roles = "Admin")]
    [HttpDelete]
    [Route("ObrisiProdavca/{id}")]
    public async Task<IActionResult> DeleteProdavac(int id)
    {
        var session = _driver.AsyncSession();
        try
        {
            int prodavnicaId = 0;
            ProdavacDTO prodavac = await session.ReadTransactionAsync(async tx =>
                {
                    var result = await tx.RunAsync("MATCH (p:Prodavac { id: " + id + " })" +
                                                    "OPTIONAL MATCH (p)-[:RADI_U]->(prod:Prodavnica) " + 
                                                    "RETURN p.name as ime, p.prezime as prezime, prod.id as pid, prod.ime as pime");
                    return await result.SingleAsync(res =>
                    {
                        if(res["pid"] == null)
                            return null;

                        prodavnicaId = res["pid"].As<int>();
                        return new ProdavacDTO()
                        {
                            Ime = res["ime"].As<string>() + " " + res["prezime"].As<string>(),
                            Prodavnica = res["pime"].As<string>()
                        };
                    });
                });

            string deleteProdavac = "MATCH (p:Prodavac { id:" + id + " }) DETACH DELETE p";
            var result = await session.WriteTransactionAsync(tx => tx.RunAsync(deleteProdavac));

            await _redisService.DeleteProdavac(prodavnicaId, prodavac);
            return StatusCode(200, "Obrisan prodavac id:" + id);
        }
        finally
        {
            await session.CloseAsync();
        }
    }

    [Authorize(Roles = "Admin")]
    [HttpGet]
    [Route("SviProdavci")]
    public async Task<List<Prodavac>> GetProdavci()
    {
        var session = _driver.AsyncSession();
        try
        {
            var list = new List<Prodavac>();
            return await session.ReadTransactionAsync(async tx =>
            {
                var result = await tx.RunAsync("MATCH (a:Prodavac) RETURN a");

                await result.ForEachAsync(res =>
                {
                    var node = res["a"].As<INode>();
                    list.Add(new Prodavac()
                    {
                        ID = Convert.ToInt16(node.Properties["id"]?.As<int>()),
                        Ime = node.Properties["name"].As<string>(),
                        Prezime = node.Properties["prezime"].As<string>(),
                        Role = node.Properties["role"].As<string>(),
                        Username = node.Properties["username"].As<string>()
                    });
                });
                return list;
            });
        }
        finally
        {
            await session.CloseAsync();
        }
    }

    [Authorize(Roles = "Admin")]
    [HttpPost]
    [Route("KreirajLinkSaProdavnicom/{idPordavnica}/{idProdavac}")]
    public async Task<IActionResult> CreateLinkProdavnicaProdavac(int idProdavnica, int idProdavac)
    {
        // statementText.Append("MATCH (p:Prodavnica {id:" + idProdavnica + "}), (o:Prodavac { id:" + idProdavac + "})" +
        //        " CREATE (p)-[r:RADI_U]->(o)");
        string statementText = "MATCH (p:Prodavac{id: $idProdavac}),(o:Prodavnica{id: $idProdavnica })" +
                                "CREATE (p)-[r:RADI_U]->(o)";
        var statementParameters = new Dictionary<string, Object>
        {
            {"idProdavnica", idProdavnica},
            {"idProdavac", idProdavac}
        };

        var Session = this._driver.AsyncSession();
        var result = await Session.WriteTransactionAsync(tx => tx.RunAsync(statementText, statementParameters));
        return StatusCode(201, "Link has been created between Prodavnica and Prodavac in the database");
    }

    [Authorize(Roles = "Admin, Prodavac")]
    [HttpGet]
    [Route("StatistikaTopProdavciKompanija/{br}")]
    public async Task<IActionResult> GetStatistikaProdavciKompanija(int br)
    {
        var statistika = await _redisService.GetStatistikaProdavciKompanijaDesc(br);
        return Ok(statistika);
    }

}