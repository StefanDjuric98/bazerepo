import React from 'react'
import DodajProizvode from '../pages/DodajProizvode';
import { useParams } from "react-router-dom";

export default function DdodajProizvode() {
    const{userId}=useParams();
    return (
        <div>
            <DodajProizvode id={userId}/>
        </div>
    )
}