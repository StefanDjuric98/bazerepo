import React, { useState } from "react";
import { makeStyles } from '@material-ui/core/styles';
import ListSubheader from '@material-ui/core/ListSubheader';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import LooksOneIcon from '@material-ui/icons/LooksOne';
import LooksTwoIcon from '@material-ui/icons/LooksTwo';
import Looks3Icon from '@material-ui/icons/Looks3';
import Looks4Icon from '@material-ui/icons/Looks4';
import Looks5Icon from '@material-ui/icons/Looks5';
import Looks6Icon from '@material-ui/icons/Looks6';
import { deepPurple } from '@material-ui/core/colors'


const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
    marginTop: 10,
    marginRight: 3
  },
  nested: {
    paddingLeft: theme.spacing(4),
  },
}));

export default function StatisitkaLista(props) {

  const classes = useStyles();

  const ikonice = [<LooksOneIcon style={{ color: deepPurple[400] }}/>, <LooksTwoIcon style={{ color: deepPurple[400] }}/>, <Looks3Icon style={{ color: deepPurple[400] }}/>, <Looks4Icon style={{ color: deepPurple[400] }}/>, <Looks5Icon style={{ color: deepPurple[400] }}/>, <Looks6Icon style={{ color: deepPurple[400] }}/>];

  return (
    <div className={classes.root}>
      <List
        component="nav"
        aria-labelledby="nested-list-subheader"
        subheader={
            <ListSubheader component="div" id="nested-list-subheader">
            <strong>{props.naslov}</strong>
            </ListSubheader>
        }
        className={classes.root}
      >
        {props.lista.map((el,i) =>
                <ListItem key={i+1}>
                    <ListItemIcon>
                        {ikonice[i%6]}
                    </ListItemIcon>
                    <ListItemText primary={el.data + " : " + el.score} />
                </ListItem>)
        }
      </List>
    </div>
  );
}