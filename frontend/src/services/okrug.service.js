import axios from "axios";
import authHeader from "./auth-header";

const API_URL = "https://localhost:5001/api/Okrug/";

const uzmiSveOkruge = () => {
    return axios.get(API_URL + "SviOkruzi", { headers: authHeader() });
  };

const uzmiOkrug = (prodavnicaId) => {
    return axios.get(API_URL + "NadjiOkrug/" + prodavnicaId, { headers: authHeader() });
};

const kreirajOkrug = (okrugDTO) => {
    return axios.post(API_URL + "KreirajOkrug", okrugDTO, { headers: authHeader() });
};

const nadjiOkrugPrekoProdavnice=(idProdavnica)=>{
  return axios.get(API_URL+"NadjiOkrug/"+idProdavnica);
};
const obrisiOkrug = (id) => {
  return axios.delete(API_URL + "ObrisiOkrug/" + id, { headers: authHeader() });
};

export default {
    uzmiSveOkruge,
    uzmiOkrug,
    kreirajOkrug,
    nadjiOkrugPrekoProdavnice,
    obrisiOkrug
  };