
namespace BazaB.Models
{

    public class Prodavnica
    {
        public int ID { get; set; }
        public string Ime { get; set; }
        public string Adresa { get; set; }
    }
}