import React, { useState, useEffect } from 'react'

import { makeStyles } from "@material-ui/core/styles";
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Checkbox from '@material-ui/core/Checkbox';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import { ValidatorForm, TextValidator} from 'react-material-ui-form-validator';
import IconButton from "@material-ui/core/IconButton";
import VisibilityIcon from "@material-ui/icons/Visibility";
import VisibilityOffIcon from "@material-ui/icons/VisibilityOff";
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import authService from '../services/auth.service';
import prodavnicaService from '../services/prodavnica.service';

function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const useStyles=makeStyles((theme)=>({
    form: {
        width: "100%", 
        marginTop: theme.spacing(1),
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
      },
    selectEmpty: {
        marginTop: theme.spacing(2),
    },
    button: {
        marginLeft: "88%",
        marginTop: "-27%",
        backgroundColor: "transparent",
    },
}))

export default function DodajProdavca() {
    const classes = useStyles();
    const [ime, SetIme]=useState('');
    const [prezime,SetPrezime]=useState('');
    const [admin, SetAdmin]=useState(false);
    const [username, SetUsername]=useState('');
    const [password, SetPassword]=useState('');
    const [showPassword, setShowPassword] = useState(false);
    const [prodavnice, setProdavnice]=useState([]);
    const [izabranaProdavnica, setIzabranaPodavnica]=useState();
    const [message, setMessage] = React.useState({open: false, type: "succes", content: ""});

    const vusername = (value) => {
        if (value.length < 3 || value.length > 20) {
          return false;
        }
        return true;
    };
    
    const vpassword = (value) => {
        if (value.length < 5 || value.length > 40) {
            return false;
        }
        return true;
    };

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
          return;
        }
    
        setMessage({ ...message, open: false });
        if(message.type === "success")
            window.location.reload();
    };

    useEffect(() => {
       
        prodavnicaService.sveProdavnice()
        .then(res=>{
            var p=res.data;
            setProdavnice(p);
        })
        .catch(error => {
            console.log(error);
        });

        ValidatorForm.addValidationRule('usernameLengthCheck', vusername);
        ValidatorForm.addValidationRule('passwordLengthCheck', vpassword);

        return () => {
            ValidatorForm.removeValidationRule('usernameLengthCheck');
            ValidatorForm.removeValidationRule('passwordLengthCheck');
        }
    }, [])

    const handleChangeIme =(ev)=>{
        SetIme(ev.target.value);
    }

    const handleChangePrezime =(ev)=>{
        SetPrezime(ev.target.value);
    }

    const handleChangeUsername =(ev)=>{
        SetUsername(ev.target.value);
    }
     
    const handleToggle=(ev)=>{
        var p=!admin;
        SetAdmin(p);
    }
    
    const handleChangeProdavnica=(ev)=>{
        //console.log(ev.target.value);
        setIzabranaPodavnica(ev.target.value);
    }

    const handleChangePassword = (ev) => {

        SetPassword(ev.target.value);
    }

    const handleClickShowPassword = () => {
      if (showPassword === false) {
        setShowPassword(true);
      } else setShowPassword(false);
    };

    const handleSubmit=(ev)=>{
        ev.preventDefault();

        let role = (admin) ? "Admin" : "Prodavac";
        const prodavac={
            ime: ime,
            prezime: prezime,
            username: username,
            role: role
        }
        console.log(prodavac);
        authService.register(prodavac, password, izabranaProdavnica.id)
            .then(res=>{
                setMessage({ open: true, type: "success", content: [res.data]});
                console.log(res);
                console.log(res.data);
                //setTimeout(window.location.reload(), 30000);
            })
            .catch(function (error) {
                console.log(error);
                setMessage({ open: true, type: "error", content: [error.response.data]});
            });
    }

    return (
        <div className={classes.form}>
            <ValidatorForm onSubmit={handleSubmit}>
                <h1 style={{ margin: 20 }}>Dodaj novog prodavca</h1>
                <div>< TextField id="standard-basic" label="Ime" variant="outlined" fullWidth style={{ margin: 8 }} onChange={handleChangeIme} /></div>
                <div>< TextField id="standard-basic" label="Prezime" variant="outlined" fullWidth style={{ margin: 8 }} onChange={handleChangePrezime} /></div>
                <div>
                    <TextValidator
                        label="Username"
                        variant="outlined"
                        fullWidth
                        style={{ margin: 8 }}
                        onChange={handleChangeUsername}
                        name="username"
                        value={username}
                        validators={['required', 'usernameLengthCheck']}
                        errorMessages={['Polje username je obavezno', 'Username mora biti između 3 i 20 karaktera']}
                    />
                </div>
                <div>
                    <TextValidator
                        label="Password"
                        variant="outlined"
                        fullWidth
                        style={{ margin: 8 }}
                        onChange={handleChangePassword}
                        name="password"
                        type={showPassword ? "text" : "password"}
                        value={password}
                        validators={['required', 'passwordLengthCheck']}
                        errorMessages={['Šifra je obavezna', 'Šifra mora biti između 5 i 40 karaktera']}
                    />
                    <IconButton
                    aria-label="toggle password visibility"
                    dge="end"
                    className={classes.button}
                    onClick={handleClickShowPassword}
                    >
                    {showPassword ? <VisibilityIcon /> : <VisibilityOffIcon />}
                    </IconButton>
                </div>
                <div style={{ margin: 8, marginTop: -9 }}>
                    <strong>Prodavac je admin</strong>
                    <Checkbox
                        edge="end"
                        onChange={handleToggle}
                        checked={admin}
                    />
                </div>
                <div>
                <FormControl variant="outlined" fullWidth className={classes.formControl}>
                    <InputLabel id="demo-simple-select-label">Prodavnica u koju se dodaje</InputLabel>
                    <Select
                        labelId="demo-simple-select-label"
                        id="demo-simple-select"
                        //value={prodavnica}
                        defaultValue=""
                        onChange={(ev)=>handleChangeProdavnica(ev)}
                        label="Prodavnica u koju se dodaje"
                    >
                        {prodavnice.map((prodavnica, ind)=>
                            <MenuItem key={ind} value={prodavnica}>{prodavnica.ime}</MenuItem>
                            )
                        }        
                    </Select>
                </FormControl>
                </div>
                <Button type="submit" color="primary" variant="contained" fullWidth style={{ margin: 8 }}>Napravi korisnika</Button>
            </ValidatorForm>

            <div className={classes.root}>
                    <Snackbar open={message.open} autoHideDuration={6000} onClose={handleClose}>
                        <Alert onClose={handleClose} severity={message.type}>
                            {message.content}
                        </Alert>
                    </Snackbar>
            </div>
        </div>
    )
}
