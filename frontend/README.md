# README #

* Neo4j baza se može preuzeti sa linka: https://elfakrs-my.sharepoint.com/:u:/g/personal/dimitrijee_elfak_rs/EeGfVgAUz15AkIo_N_uiOxsB2VAd2018yikCeGIPYtTU8g?e=17dTw9
* Redis baza je dostupna na lokaciji '..\bazerepo\Redis-x64-3.2.100'.

* Za otvaranje aplikacije po mogućstvu koristiti Firefox jer Chrome blokira aplikaciju zbog SSL sertifikata.
* Backend se pokreće naredbom 'dotnet run' iz konzole sa lokacije '..\bazerepo\frontend'. Ulkoliko nisu instalirani paketi potrebno je izvršiti i naredbu 'dotnet restore'.
* Frontend se pokreće naredbom 'npm start' iz konzole sa lokacije '..\bazerepo\backend\BazaB'. Ulkoliko nisu instalirani paketi potrebno je izvršiti i naredbu 'npm install'.

### Users ###

* Ime (username : password)

* Dušan Pavlović (admin : admin)
# P1
* Vuk Stojanović (vuk : vuk123)

# P2
* Borivoje Šurdilović (surda : Surda)
* Jelena Stanković (jeka : jeka123)
# P3
* Dimitra Anđelković (dimitra : dida123)
# P4
* Milomir Petković (milo : Milo123)
* Andrija Stefanović (aki : Aki123)

# P5
* Goran Petrović (goki: goki123)

# P6
* Milica Milošević (mica : mica123)

# P7
* Andrija Nedeljković (andra : aki123)

# P8
* Davorin Bogović (davor : crnoBijeliSvet)
* Marinko Madžgalj (marinko : marinko)
* Nikola Petrović (nidza : Nidza)

# P9
* Stefan Marković (stefko : Stefko123)
* Miloš Petković (losmi : Misa123)
* Stevan Surdulić (steva : Steva123)

# P12
* Miodrag Petković (miki : veliki)
* Marko Radosavljević (mare : mare123)

# P13
* Filip Jovanović (fica : Fica123)

# P14
* Stanko Petrović (stane : Stane)

# P15
* Veljko Stojanović (velja : veljac)
* Vladimir Jovanović (vlada : Vladica)
## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: [https://facebook.github.io/create-react-app/docs/code-splitting](https://facebook.github.io/create-react-app/docs/code-splitting)

### Analyzing the Bundle Size

This section has moved here: [https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size](https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size)

### Making a Progressive Web App

This section has moved here: [https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app](https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app)

### Advanced Configuration

This section has moved here: [https://facebook.github.io/create-react-app/docs/advanced-configuration](https://facebook.github.io/create-react-app/docs/advanced-configuration)

### Deployment

This section has moved here: [https://facebook.github.io/create-react-app/docs/deployment](https://facebook.github.io/create-react-app/docs/deployment)

### `npm run build` fails to minify

This section has moved here: [https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify](https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify)
