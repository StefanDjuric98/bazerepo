namespace BazaB.Models
{
    public class GetStanjeDTO
    {
        public int ProizvodId { get; set; }
        public string ProizvodNaziv { get; set; }
        public int DostupnaKolicina { get; set; }
        public Prodavnica Prodavnica { get; set; }
        public string Poruka { get; set; }
    }
}