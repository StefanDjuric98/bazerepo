import React from 'react'
import { useParams } from "react-router-dom";
import EditProdavnica from '../pages/EditProdavnica';


export default function EEditProdavnica() {
    const { prodavnicaId } = useParams();
    return (
        <div>
            <EditProdavnica id={prodavnicaId}/>
        </div>
    )
}