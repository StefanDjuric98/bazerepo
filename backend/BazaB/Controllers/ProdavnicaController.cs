using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Neo4j.Driver;
using System.Text;
using BazaB.Models;
using System.Net.Http;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;

[Route("api/[controller]")]
[ApiController]
public class ProdavnicaController : ControllerBase
{
    private readonly IDriver _driver;
    private readonly RedisService _redisService;

    public ProdavnicaController(IDriver driver, RedisService redisService)
    {
        _driver = driver;
        _redisService = redisService;
    }

    [Authorize(Roles = "Admin")]
    [HttpPost]
    [Route("KreirajProdavnicu")]
    public async Task<IActionResult> CreateProdavnica(CreateProdavnicaDTO prodavnicaDTO)
    {
        var session = this._driver.AsyncSession();
        try
        {
            bool prodavnicaExists = await session.ReadTransactionAsync(async tx =>
            {
                var result = await tx.RunAsync("MATCH (p:Prodavnica { ime: '" + prodavnicaDTO.Prodavnica.Ime + "' }) RETURN count(*) as count");

                return await result.SingleAsync(res =>
                {
                    return res["count"].As<int>() > 0;
                });
            });

            if (prodavnicaExists)
                return BadRequest("Prodavnica: " + prodavnicaDTO.Prodavnica.Ime + " already exists in the database!");

            int id = await NextIdHelper.GetId(_driver);
            string statementText = ("CREATE ( p:Prodavnica {id: $id ,ime: $ime, adresa: $adresa})");
            var statementParameters = new Dictionary<string, Object>
            {
                {"id", id},
                {"ime", prodavnicaDTO.Prodavnica.Ime },
                {"adresa", prodavnicaDTO.Prodavnica.Adresa}
            };

            var result = await session.WriteTransactionAsync(tx => tx.RunAsync(statementText, statementParameters));

            await CreateLinkProdavnicaOkrug(id, prodavnicaDTO.OkrugId);
            return Ok("Node Prodavnica with id: " + id + " has been created in the database");
        }
        finally
        {
            await session.CloseAsync();
        }


    }

    [Authorize(Roles = "Admin, Prodavac")]
    [HttpGet]
    [Route("UzmiProdavnicu/{id}")]
    public async Task<GetProdavnicaDTO> GetProdavnice(int id)
    {
        var session = _driver.AsyncSession();
        try
        {
            return await session.ReadTransactionAsync(async tx =>
            {
                GetProdavnicaDTO prodavnica;
                var result = await tx.RunAsync("MATCH (p:Prodavnica { id:" + id + " })-[:PRIPADA]->(o:Okrug) RETURN p, o.ime as okrug");

                prodavnica = await result.SingleAsync(res =>
                {

                    var node = res["p"].As<INode>();
                    return new GetProdavnicaDTO()
                    {
                        ID = Convert.ToInt16(node.Properties["id"]?.As<int>()),
                        Ime = node.Properties["ime"].As<string>(),
                        Adresa = node.Properties["adresa"].As<string>(),
                        Okrug = res["okrug"].As<string>()
                    };
                });
                return prodavnica;
            });
        }
        finally
        {
            await session.CloseAsync();
        }
    }

    [Authorize(Roles = "Admin,Prodavac")]
    [HttpGet]
    [Route("SveProdavnice")]
    public async Task<List<Prodavnica>> GetProdavnice()
    {
        var session = _driver.AsyncSession();
        try
        {
            var list = new List<Prodavnica>();
            return await session.ReadTransactionAsync(async tx =>
            {
                var result = await tx.RunAsync("MATCH (a:Prodavnica) RETURN a ORDER BY a.ime");

                await result.ForEachAsync(res =>
                {
                    var node = res["a"].As<INode>();
                    list.Add(new Prodavnica()
                    {
                        ID = Convert.ToInt16(node.Properties["id"]?.As<int>()),
                        Ime = node.Properties["ime"].As<string>(),
                        Adresa = node.Properties["adresa"].As<string>()
                    });
                });
                return list;
            });
        }
        finally
        {
            await session.CloseAsync();
        }
    }

    [Authorize(Roles = "Admin")]
    [HttpPut]
    [Route("IzmeniProdavnicu")]
    public async Task<IActionResult> EditProdavnica(Prodavnica prodavnica)
    {
        var session = _driver.AsyncSession();
        try
        {
            var statementParameters = new Dictionary<string, Object>
            {
                {"id", prodavnica.ID},
                {"ime", prodavnica.Ime},
                {"adresa", prodavnica.Adresa}
            };
            string statementText = "MATCH (p:Prodavnica {id: $id}) " +
                        "SET p.ime = $ime, p.adresa = $adresa";

            await session.WriteTransactionAsync(tx => tx.RunAsync(statementText, statementParameters));
            return Ok("Prodavnica has been updated!");
        }
        finally
        {
            await session.CloseAsync();
        }
    }

    [Authorize(Roles = "Admin")]
    [HttpDelete]
    [Route("ObrisiProdavnicu/{id}")]
    public async Task<IActionResult> DeleteProdavnica(int id)
    {
        var session = _driver.AsyncSession();
        try
        {
            string prodavnicaIme = string.Empty;
            List<ProdavacDTO> prodavci = new List<ProdavacDTO>();
            await session.ReadTransactionAsync(async tx =>
            {
                var result = await tx.RunAsync("MATCH (prod:Prodavnica {id: " + id + "}) " +
                                                "OPTIONAL MATCH (porod)<-[:RADI_U]-(p:Prodavac)" + 
                                                "RETURN p.name as ime, p.prezime as prezime, prod.ime as pime");
                return await result.ForEachAsync(res =>
                {
                    if(res["ime"] == null)
                    {
                        prodavnicaIme = res["pime"].As<string>();
                        return;
                    }

                    if(prodavnicaIme == String.Empty)
                            prodavnicaIme = res["pime"].As<string>();
                        
                    prodavci.Add(new ProdavacDTO()
                    {
                        Ime = res["ime"].As<string>() + " " + res["prezime"].As<string>(),
                        Prodavnica = res["pime"].As<string>()
                    });
                });
            });

            string deleteProdavnica = "MATCH (p:Prodavnica { id:" + id + " })" +
                                    "OPTIONAL MATCH (p)<-[v:RADI_U]-(prod:Prodavac) DETACH DELETE prod, p";

            await session.WriteTransactionAsync(tx => tx.RunAsync(deleteProdavnica));

            Prodavnica p = new Prodavnica 
            {
                ID = id,
                Ime = prodavnicaIme
            };
            await _redisService.DeleteProdavnica(p, prodavci);
            
            return Ok("Obrisana prodavnica id:" + id);
        }
        finally
        {
            await session.CloseAsync();
        }
    }

    [Authorize(Roles = "Admin,Prodavac")]
    [HttpPost]
    [Route("KreirajLinkProizvod")]
    public async Task<IActionResult> CreateLinkProizvod(Stanje stanje)
    {
        var session = this._driver.AsyncSession();
        try
        {
            string authenticatedRole = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Role).Value;
            int prodavnicaId = int.Parse(User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.GroupSid).Value);

            var statementParameters = new Dictionary<string, Object>
            {
                {"idProdavnica", prodavnicaId},
                {"idProizvod", stanje.Proizvod.ID},
                {"kolicina", stanje.Kolicina },
                {"minKolicina", stanje.MinKolicina}
            };

            if (authenticatedRole == "Admin")
                statementParameters["idProdavnica"] = stanje.Prodavnica.ID;

            bool linkExists = await session.ReadTransactionAsync(async tx =>
            {
                var result = await tx.RunAsync("MATCH (p:Prodavnica { id: $idProdavnica })-[v:IMA]->(proiz:Proizvod {id: $idProizvod})" +
                                                " RETURN count(*) as count", statementParameters);

                return await result.SingleAsync(res =>
                {
                    return res["count"].As<int>() > 0;
                });
            });

            string statementText;
            if (linkExists)
            {
                statementText = @"MATCH (p:Prodavnica {id: $idProdavnica})-[v:IMA]->(proiz:Proizvod {id: $idProizvod}) 
                                SET v.kolicina = v.kolicina + $kolicina, v.minKolicina = $minKolicina, proiz.kolicina = proiz.kolicina - $kolicina";

                await session.WriteTransactionAsync(tx => tx.RunAsync(statementText, statementParameters));
                return Ok("Link has been updated between Prodavnica and Proizvod: " + stanje.Proizvod.Naziv);
            }

            //MATCH (p:Prodavnica { id: 1 }), (pr:Proizvod { id: 2 }) CREATE (p)-[r:IMA { id: 3, kolicina: 10, minKolicina: 2 }]->(pr)
            statementText = (@"MATCH (p:Prodavnica {id: $idProdavnica}), (pr:Proizvod { id: $idProizvod }) 
                                CREATE (p)-[r:IMA { kolicina: $kolicina, minKolicina: $minKolicina}]->(pr)
                                SET pr.kolicina = pr.kolicina - $kolicina");

            await session.WriteTransactionAsync(tx => tx.RunAsync(statementText, statementParameters));
            return Ok("Link has been created between Proizvod id: " + stanje.Proizvod.ID + " and Prodavnica id: " + statementParameters["idProdavnica"] + " in the database");
        }
        finally
        {
            await session.CloseAsync();
        }
    }

    [Authorize(Roles = "Admin,Prodavac")]
    [HttpGet]
    [Route("NadjiProizvod/{idProizvod}/{idProdavnica}")]
    public async Task<ServiceResponse<LinkProdavnicaProizvod>> GetLinkProizvod(int idProdavnica, int idProizvod)
    {
        var session = _driver.AsyncSession();

        try
        {

            bool proizvodExists = await session.ReadTransactionAsync(async tx =>
            {
                var result = await tx.RunAsync("MATCH (n {id:" + idProdavnica + "})-[r:IMA]->(c{id:" + idProizvod + "}) RETURN count(*) as count");
                return await result.SingleAsync(res =>
                {
                    return res["count"].As<int>() > 0;
                });
            });

            if (!proizvodExists)
            {
                return new ServiceResponse<LinkProdavnicaProizvod>()
                {
                    Data = null,
                    Success = false,
                    Message = "Proizvoda nema na stanju u ovoj prodavnici!"
                };
            }
            else
            {
                LinkProdavnicaProizvod link = null;
                await session.ReadTransactionAsync(async tx =>
                {
                    var result = await tx.RunAsync("MATCH (n {id:" + idProdavnica + "})-[r:IMA]->(c{id:" + idProizvod + "}) RETURN r");

                    link = await result.SingleAsync(res =>
                    {
                        var node = res["r"].As<IRelationship>();
                        return new LinkProdavnicaProizvod()
                        {
                            Kolicina = node.Properties["kolicina"].As<int>(),
                            MinKolicina = node.Properties["minKolicina"].As<int>(),
                        };
                    });
                });

                return new ServiceResponse<LinkProdavnicaProizvod>()
                {
                    Data = link,
                    Success = true,
                    Message = "Proizvod je pronađen!"
                };
            }

        }
        finally
        {
            await session.CloseAsync();
        }
    }

    [HttpPut]
    [Route("UpdateLinkProizovdProdavnica/{idProizvod}/{idProdavnica}")]
    public async Task<IActionResult> PutLinkProdavnicaProizvod(LinkProdavnicaProizvod link, int idProizvod, int idProdavnica)
    {
        var statementText = new StringBuilder();
        statementText.Append("MATCH (n {id:" + idProdavnica + "})-[r:IMA]->(c{id:" + idProizvod + "}) SET r.kolicina=" + link.Kolicina + ",r.minKolicina=" + link.MinKolicina);

        var statementParameters = new Dictionary<string, Object>
        {
            {"idProizvod", idProizvod},
            {"idProdavnica", idProdavnica},
            {"kolicina", link.Kolicina},
            {"minKolicina", link.MinKolicina}
        };
        var session = this._driver.AsyncSession();
        var result = await session.WriteTransactionAsync(tx => tx.RunAsync(statementText.ToString(), statementParameters));
        return StatusCode(201, "link has been updated between Prodavnica and Proizvod");
    }

    [Authorize(Roles = "Admin")]
    [HttpPost]
    [Route("KreirajLinkSaOkrugom/{idProdavnica}/{idOkrug}")]
    public async Task<IActionResult> CreateLinkProdavnicaOkrug(int idProdavnica, int idOkrug)
    {
        var statementText = new StringBuilder();
        statementText.Append("MATCH (p:Prodavnica {id:" + idProdavnica + "}), (o:Okrug { id:" + idOkrug + "})" +
                                "CREATE (p)-[r:PRIPADA]->(o)");

        var statementParameters = new Dictionary<string, Object>
        {
            {"idProdavnica", idProdavnica},
            {"idOkrug", idOkrug}
        };

        var session = this._driver.AsyncSession();
        var result = await session.WriteTransactionAsync(tx => tx.RunAsync(statementText.ToString(), statementParameters));
        return StatusCode(201, "Link has been created between Prodavnica and Okrug in the database");

    }

    [Authorize(Roles = "Admin,Prodavac")]
    [HttpPost]
    [Route("NadjiProdavnicuSaOdredjenimProizvodom")]
    public async Task<ServiceResponse<GetStanjeDTO>> GetProdavnicaSaProizvodom(CheckStanjeDTO checkStanje)
    {
        var session = _driver.AsyncSession();
        try
        {
            var statementParameters = new Dictionary<string, Object>
            {
                {"proizvodId", checkStanje.Proizvod.ID},
                {"prodavnicaId", checkStanje.ProdavnicaId},
                {"okrugId", checkStanje.OkrugId}
            };

            bool proizvodAvailable = await session.ReadTransactionAsync(async tx =>
            {
                var result = await tx.RunAsync("MATCH (p:Proizvod { id: $proizvodId })<-[stanje:IMA]-(pro:Prodavnica) " +
                                                "WHERE (stanje.kolicina - 1) > stanje.minKolicina RETURN count(*) as count", statementParameters);

                return await result.SingleAsync(res =>
                {
                    return res["count"].As<int>() > 0;
                });
            });

            if (!proizvodAvailable)
                return new ServiceResponse<GetStanjeDTO>()
                {
                    Data = null,
                    Success = false,
                    Message = "Ovaj proizvod trenutno nije dostupan ni u jednoj prodavnici u dovoljnoj količini!"
                };

            Prodavnica prodavnica = null;
            Stanje stanje = null;
            await session.ReadTransactionAsync(async tx =>
            {
                var result = await tx.RunAsync("OPTIONAL MATCH (start:Okrug {id: $okrugId })<-[:PRIPADA]-(p:Prodavnica)-[stanje:IMA]->(proiz:Proizvod {id: $proizvodId}) " +
                                            "WHERE p.id <> $prodavnicaId and (stanje.kolicina - 1) > stanje.minKolicina RETURN p, stanje LIMIT 1", statementParameters);

                prodavnica = await result.SingleAsync(res =>
                {
                    if (res["p"] == null)
                        return null;

                    var rel = res["stanje"].As<IRelationship>();
                    stanje = new Stanje()
                    {
                        Kolicina = rel.Properties["kolicina"].As<int>(),
                        MinKolicina = rel.Properties["minKolicina"].As<int>()
                    };

                    var node = res["p"].As<INode>();
                    return new Prodavnica()
                    {
                        ID = Convert.ToInt16(node.Properties["id"]?.As<int>()),
                        Ime = node.Properties["ime"].As<string>(),
                        Adresa = node.Properties["adresa"].As<string>()
                    };
                });
            });
            if (prodavnica != null)
                return new ServiceResponse<GetStanjeDTO>()
                {
                    Data = new GetStanjeDTO()
                    {
                        ProizvodId = checkStanje.Proizvod.ID,
                        ProizvodNaziv = checkStanje.Proizvod.Naziv,
                        DostupnaKolicina = stanje.Kolicina,
                        Prodavnica = prodavnica,
                        Poruka = String.Format("Možete poručiti proizvod: {0} iz prodavnice: {1} koja pripada istom okrugu na adresi: {2} \nKoličina dostupnog proizvoda: {3} \nIzaberite količinu koju želite da naručite:",
                                                checkStanje.Proizvod.Naziv, prodavnica.Ime, prodavnica.Adresa, stanje.Kolicina)
                    },
                    Success = true,
                    Message = String.Format("Možete poručiti proizvod iz prodavnice: {1} koja pripada istom okrugu na adresi: {2} \nKoličina dostupnog proizvoda: {3}",
                                                checkStanje.Proizvod.Naziv, prodavnica.Ime, prodavnica.Adresa, stanje.Kolicina)
                };

            string okrug = null;
            int udaljenost = 0;
            await session.ReadTransactionAsync(async tx =>
            {
                var result = await tx.RunAsync(@"MATCH (n:Okrug {id: $okrugId })
                                                CALL gds.alpha.shortestPath.deltaStepping.stream({ 
                                                    nodeProjection: 'Okrug',
                                                    relationshipProjection: {
                                                        POVEZAN: {
                                                        type: 'POVEZAN',
                                                        properties: 'razdaljina',
                                                        orientation: 'UNDIRECTED'
                                                        } 
                                                    },
                                                    startNode: n,
                                                    relationshipWeightProperty: 'razdaljina',
                                                    delta: 3.0
                                                    })
                                                YIELD nodeId, distance
                                                MATCH (o:Okrug {id: gds.util.asNode(nodeId).id})<-[:PRIPADA]-(p:Prodavnica)-[stanje:IMA]->(pro:Proizvod {id: $proizvodId})
                                                WHERE o.id = gds.util.asNode(nodeId).id and o.id <> n.id and (stanje.kolicina - 1) > stanje.minKolicina
                                                RETURN p as prodavnica, stanje, gds.util.asNode(nodeId).ime AS okrug, distance AS udaljenost
                                                ORDER BY distance
                                                LIMIT 1", statementParameters);

                prodavnica = await result.SingleAsync(res =>
                {
                    var rel = res["stanje"].As<IRelationship>();
                    stanje = new Stanje()
                    {
                        Kolicina = rel.Properties["kolicina"].As<int>(),
                        MinKolicina = rel.Properties["minKolicina"].As<int>()
                    };

                    okrug = res["okrug"].As<string>();
                    udaljenost = res["udaljenost"].As<int>();

                    var node = res["prodavnica"].As<INode>();
                    return new Prodavnica()
                    {
                        ID = Convert.ToInt16(node.Properties["id"]?.As<int>()),
                        Ime = node.Properties["ime"].As<string>(),
                        Adresa = node.Properties["adresa"].As<string>()
                    };
                });
            });

            return new ServiceResponse<GetStanjeDTO>()
            {
                Data = new GetStanjeDTO()
                {
                    ProizvodId = checkStanje.Proizvod.ID,
                    ProizvodNaziv = checkStanje.Proizvod.Naziv,
                    DostupnaKolicina = stanje.Kolicina,
                    Prodavnica = prodavnica,
                    Poruka = String.Format("Možete poručiti proizvod '{0}' iz prodavnice '{1}' koja pripada okrugu '{2}' na adresi '{3}' i nalazi se na udaljenosti od {4} km. \nKoličina dostupnog proizvoda: {5} \nIzaberite količinu koju želite da naručite:",
                                            checkStanje.Proizvod.Naziv, prodavnica.Ime, okrug, prodavnica.Adresa, udaljenost, stanje.Kolicina)
                },
                Success = true,
                Message = String.Format("Možete poručiti proizvod iz prodavnice: {1} koja pripada okrugu: {2} na adresi: {3} i nalazi se na udaljenosti od {4} km. \nKoličina dostupnog proizvoda: {5}",
                                            checkStanje.Proizvod.Naziv, prodavnica.Ime, okrug, prodavnica.Adresa, udaljenost, stanje.Kolicina)
            };
        }
        finally
        {
            await session.CloseAsync();
        }
    }

    [Authorize(Roles = "Admin,Prodavac")]
    [HttpPut]
    [Route("IzvrsiProdaju")]
    public async Task<ServiceResponse<List<GetStanjeDTO>>> PutProdaja(ProdajaDTO prodaja)
    {
        int authorizedId = int.Parse(User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.GroupSid).Value);
        if (authorizedId != prodaja.ProdavnicaId)
            return new ServiceResponse<List<GetStanjeDTO>>()
            {
                Data = null,
                Success = false,
                Message = "Nije moguće izvršiti prodaju u prodavnici kojoj prodvac ne pripada!"
            };

        string statementText = @"MATCH (p:Prodavnica {id: $idProdavnica})-[s:IMA]->(pro: Proizvod {id: $idProizvod}) 
                                SET s.kolicina = s.kolicina - $kolicina
                                RETURN s.kolicina < s.minKolicina as trazi";

        var statementParameters = new Dictionary<string, Object>
        {
            {"idProdavnica", prodaja.ProdavnicaId},
            {"idProizvod", 0},
            {"kolicina", 0}
        };

        var session = this._driver.AsyncSession();
        StringBuilder message = new StringBuilder("Prodaja uspešno izvršena! Sledećih proizvoda nema dovoljno na stanju: ");
        List<GetStanjeDTO> data = new List<GetStanjeDTO>();
        bool success = false;
        try
        {
            foreach (Stanje p in prodaja.ProizvodiZaProdaju)
            {
                statementParameters["idProizvod"] = p.Proizvod.ID;
                statementParameters["kolicina"] = p.Kolicina;

                bool search = false;
                await session.WriteTransactionAsync(async tx =>
                {
                    var result = await tx.RunAsync(statementText, statementParameters);

                    search = await result.SingleAsync(res =>
                    {
                        return res["trazi"].As<bool>();
                    });
                });
                if (search)
                {
                    var checkStanje = new CheckStanjeDTO()
                    {
                        Proizvod = p.Proizvod,
                        ProdavnicaId = prodaja.ProdavnicaId,
                        OkrugId = prodaja.OkrugId
                    };
                    var response = await GetProdavnicaSaProizvodom(checkStanje);
                    message.Append("\n " + p.Proizvod.Naziv + ": " + response.Message);
                    data.Add(response.Data);
                    success = success || response.Success;
                }
            }

            await _redisService.UpdateStatistika(prodaja);

            return new ServiceResponse<List<GetStanjeDTO>>()
            {
                Data = data,
                Success = success,
                Message = message.ToString()
            };
        }
        finally
        {
            await session.CloseAsync();
        }
    }

    [Authorize(Roles = "Admin, Prodavac")]
    [HttpPost]
    [Route("PosaljiNarudzbenicu/{prodavnica}")]
    public async Task<IActionResult> PostNarudzbenica(Obavestenje obavestenje, string prodavnica)
    {
        obavestenje.Poruka = String.Format("Prodavnica '{0}' želi da naruči {1} kom. '{2}'.",
                                            prodavnica, obavestenje.ZahtevanaKolicina, obavestenje.ProizvodNaziv);
        await _redisService.PostObavestenje(obavestenje);
        return Ok(obavestenje);
    }

    [Authorize(Roles = "Admin, Prodavac")]
    [HttpGet]
    [Route("UzmiObavestenja/{id}")]
    public async Task<IActionResult> GetObavestenja(int id)
    {
        List<Obavestenje> obavestenja = await _redisService.GetObavestenja(id);
        return Ok(obavestenja);
    }

    [Authorize(Roles = "Admin, Prodavac")]
    [HttpPut]
    [Route("ObradiNarudzbenicu/{prodavnica}/{prihvaceno}")]
    public async Task<IActionResult> PostObradiNarudzbenicu(Obavestenje obavestenje, bool prihvaceno, string prodavnica)
    {
        string poruka;
        if(prihvaceno)
        {
            poruka = String.Format("Prodavnica '{0}' je prihvatila vašu narudžbenicu za {1} kom. '{2}'.",
                                            prodavnica, obavestenje.ZahtevanaKolicina, obavestenje.ProizvodNaziv);
            
            var session = _driver.AsyncSession();
            try
            {
                string statementText = "MATCH (pIz:Prodavnica {id: $idIz})-[sIz:IMA]->(pro: Proizvod {id: $idProizvod}) " +
                                        "OPTIONAL MATCH (pro)<-[sU:IMA]-(pU:Prodavnica {id: $idU}) " +
                                        "SET sIz.kolicina = sIz.kolicina - $kolicina " +
                                        "SET sU.kolicina = sU.kolicina + $kolicina " +
                                        "RETURN sU as stanje";
                
                var statementParameters = new Dictionary<string, Object>
                {
                    {"idIz", obavestenje.ZaId},
                    {"idU", obavestenje.OdId},
                    {"idProizvod", obavestenje.ProizvodId},
                    {"kolicina", obavestenje.ZahtevanaKolicina}
                };

                bool createLink = false;
                await session.WriteTransactionAsync(async tx =>
                {
                    var result = await tx.RunAsync(statementText, statementParameters);

                    createLink = await result.SingleAsync(res =>
                    {
                        return res["stanje"] == null;
                    });
                });
                if(createLink)
                {
                    statementText = "MATCH (p:Prodavnica {id: $idU}), (pr:Proizvod { id: $idProizvod }) " +
                                    "CREATE (p)-[r:IMA { kolicina: $kolicina, minKolicina: 1}]->(pr)";

                    await session.WriteTransactionAsync(tx => tx.RunAsync(statementText, statementParameters));
                }
            }
            catch(Exception e)
            {
                return BadRequest("Neo4j query failed!\n" + e.StackTrace);
            }
            finally
            {
                await session.CloseAsync();
            }
        }
        else
            poruka = String.Format("Prodavnica '{0}' je odbila vašu narudžbenicu za {1} kom. '{2}'.",
                                            prodavnica, obavestenje.ZahtevanaKolicina, obavestenje.ProizvodNaziv);

        Obavestenje odgovor = new Obavestenje()
        {
            Poruka = poruka,
            OdId = obavestenje.ZaId,
            ZaId = obavestenje.OdId
        };
        await _redisService.PostObavestenje(odgovor);
        long res = await _redisService.DeleteObavestenje(obavestenje);
        return Ok("Uspešno obrađena narudžbenica!");
    }

    [Authorize(Roles = "Admin, Prodavac")]
    [HttpPut]
    [Route("UkloniObavestenje")]
    public async Task<IActionResult> DeleteObavestenje(Obavestenje obavestenje)
    {
        await _redisService.DeleteObavestenje(obavestenje);
        return Ok("Obaveštenje uspešno uklonjeno!");
    }

    [Authorize(Roles = "Admin, Prodavac")]
    [HttpGet]
    [Route("StatistikaProizvodiProdavnica/{id}/{br}")]
    public async Task<IActionResult> GetStatistikaProizvodiProdavnica(int id, int br)
    {
        string key = "proizvodi:" + id + ":prodavnica";
        var statistika = await _redisService.GetStatistikaDesc(key, br);
        return Ok(statistika);
    }

    [Authorize(Roles = "Admin, Prodavac")]
    [HttpGet]
    [Route("StatistikaProdavciProdavnica/{id}/{br}")]
    public async Task<IActionResult> GetStatistikaProdavciProdavnica(int id, int br)
    {
        string key = "prodavci:" + id + ":prodavnica";
        var statistika = await _redisService.GetStatistikaDesc(key, br);
        return Ok(statistika);
    }

    [Authorize(Roles = "Admin, Prodavac")]
    [HttpGet]
    [Route("StatistikaTopProfitProdavnice/{br}")]
    public async Task<IActionResult> GetStatistikaTopProdavnice(int br)
    {
        string key = "top:prodavnice";
        var statistika = await _redisService.GetStatistikaDesc(key, br);
        return Ok(statistika);
    }

}