
namespace BazaB.Models
{

    public class CreateProdavacDTO
    {
        public Prodavac Prodavac { get; set; }
        public string Password { get; set; }
        public int ProdavnicaId { get; set; }
    }

}