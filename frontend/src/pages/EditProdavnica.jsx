
import React, { useState, useEffect } from 'react'
import { useHistory } from "react-router-dom";

//mui
import { makeStyles } from "@material-ui/core/styles";
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import prodavnicaService from '../services/prodavnica.service';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';

function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const useStyles = makeStyles((theme) => ({
    formControl: {
      margin: theme.spacing(1),
      minWidth: 120
    },
    form: {
        width: "100%", 
        marginTop: theme.spacing(1),
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
    },
    selectEmpty: {
      marginTop: theme.spacing(2)
    }
  }));

 export default function EditProdavnica(props) {
    const classes = useStyles();
 
    let history = useHistory();
    const [ime, setIme]=useState('');
    const [adresa, setAdresa]=useState("");
    const [okrug, setOkrug]=useState("");
    const [message, setMessage] = React.useState({open: false, type: "succes", content: ""});

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
          return;
        }
    
        setMessage({ ...message, open: false });
        if(message.type === "success")
        {
            history.push("/Prodaja");
        }
            
    };

    useEffect(() => {
        prodavnicaService.uzmiJednuProdavnicu(props.id)
            .then(res => {
                var prodavnica=res.data
                setIme(prodavnica.ime);
                setAdresa(prodavnica.adresa);
                setOkrug(prodavnica.okrug)
            }).catch(error => {
                console.log(error);
            });
        return () => {};
      }, [props.id]);
        

        const handleChangeIme=(ev)=>{
            setIme(ev.target.value);
        }

        const handleChangeAdresa=(ev)=>{
            setAdresa(ev.target.value);
        }

        const handleSubmit=(ev)=>{
            ev.preventDefault();

            const prodavnica={
                id: props.id,
                ime: ime,
                adresa:adresa
            };

            prodavnicaService.izmeniProdavnicu(prodavnica)
            .then(res=>{
                console.log(res);
                console.log(res.data);
                setMessage({ open: true, type: "success", content: [res.data]});
            })
            .catch(function (error) {
                console.log(error);
                setMessage({ open: true, type: "error", content: [error.response.data]});
            });
        }
    
        return (
            <div className={classes.form}>
                
                <form onSubmit={handleSubmit}>
                    <h1 style={{ margin: 20 }}>Izmeni podatke o prodavnici</h1>
                    <div>< TextField id="standard-basic" variant="outlined" fullWidth disabled style={{ margin: 8 }} label="Okrug" value={okrug} /></div>
                    <div>< TextField id="standard-basic" variant="outlined" fullWidth style={{ margin: 8 }} label="Ime prodavnice" value={ime} onChange={handleChangeIme} /></div>
                    <div>< TextField id="standard-basic" variant="outlined" fullWidth style={{ margin: 8 }} label="Adresa prodavnice" value={adresa} onChange={handleChangeAdresa} /></div>
                    <Button variant="contained" color="primary" fullWidth style={{ margin: 8 }} type="submit">
                        Izmeni 
                    </Button>
                </form>

                <div className={classes.root}>
                    <Snackbar open={message.open} autoHideDuration={3500} onClose={handleClose}>
                        <Alert onClose={handleClose} severity={message.type}>
                            {message.content}
                        </Alert>
                    </Snackbar>
            </div>
            </div>
        )
    
};


