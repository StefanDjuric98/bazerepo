
import React, { useState, useEffect } from 'react'

//mui
import { makeStyles } from "@material-ui/core/styles";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import InputLabel from "@material-ui/core/InputLabel";
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import okrugService from '../services/okrug.service';
import prodavnicaService from '../services/prodavnica.service';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';

function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const useStyles = makeStyles((theme) => ({
    formControl: {
      margin: theme.spacing(1),
      minWidth: 120
    },
    form: {
        width: "100%", 
        marginTop: theme.spacing(1),
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
    },
    selectEmpty: {
      marginTop: theme.spacing(2)
    }
  }));

 export default function KreirajProdavnicu() {
    const classes = useStyles();
 
    const [ime, setIme]=useState('');
    const [adresa, setAdresa]=useState("");
    const [okruzi, setOkruzi]=useState([]);
    const [okrug, setOkrug]=useState("");
    const [message, setMessage] = React.useState({open: false, type: "succes", content: ""});

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
          return;
        }
    
        setMessage({ ...message, open: false });
        if(message.type === "success")
            window.location.reload();
    };

    useEffect(() => {
        okrugService.uzmiSveOkruge()
            .then(res =>{
                var okruzi=res.data
                setOkruzi(okruzi);
            }).catch(function (error) {
                console.log(error);
            })
        return () => {};
      }, []);
        

        //menjanje state-a
        const handleChangeIme=(ev)=>{
            setIme(ev.target.value);
        }
        const handleChangeAdresa=(ev)=>{
            setAdresa(ev.target.value);
        }

        const handleChangeOkrug=(ev)=>{
            setOkrug(ev.target.value);
        }

        //dodavanje prodavnice
        const handleSubmit=(ev)=>{
            ev.preventDefault();

            const prodavnica={
                ime: ime,
                adresa:adresa, 
            };

            console.log(okrug);

            prodavnicaService.kreirajProdavnicu(prodavnica, okrug)
            .then(res=>{
                console.log(res);
                console.log(res.data);
                setMessage({ open: true, type: "success", content: [res.data]});
            })
            .catch(function (error) {
                console.log(error);
                setMessage({ open: true, type: "error", content: [error.response.data]});
            });
        }
    
        return (
            <div className={classes.form}>
                
                <form onSubmit={handleSubmit}>
                    <h1 style={{ margin: 20 }}>Dodaj novu prodavnicu</h1>
                    <FormControl variant="outlined" fullWidth style={{ margin: 8 }} className={classes.formControl}>
                        <InputLabel >Okrug</InputLabel>
                        <Select
                            value={okrug}
                            label="Okrug"
                            fullWidth
                            onChange={handleChangeOkrug}
                            >
                                {okruzi.map(okrug => (
                                <MenuItem key={okrug.id}  value={okrug.id}>
                                {okrug.ime}
                                </MenuItem>
                            ))}
                            </Select>
                    </FormControl>

                    <div>< TextField id="standard-basic" variant="outlined" fullWidth style={{ margin: 8 }} label="Ime prodavnice" onChange={handleChangeIme} /></div>
                    <div>< TextField id="standard-basic" variant="outlined" fullWidth style={{ margin: 8 }} label="Adresa prodavnice" onChange={handleChangeAdresa} /></div>
                    <Button variant="contained" color="primary" fullWidth style={{ margin: 8 }} type="submit">
                        Kreiraj prodavnicu
                    </Button>
                </form>

                <div className={classes.root}>
                    <Snackbar open={message.open} autoHideDuration={5000} onClose={handleClose}>
                        <Alert onClose={handleClose} severity={message.type}>
                            {message.content}
                        </Alert>
                    </Snackbar>
            </div>
            </div>
        )
    
};


