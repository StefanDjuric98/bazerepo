using System.Text.Json;

namespace BazaB.Models
{
    public class Obavestenje
    {
        public bool Zahtev { get; set; } = false;
        public string Poruka { get; set; } = null;
        public int OdId { get; set; }
        public int ZaId { get; set; }
        public int ProizvodId { get; set; } = 0;
        public string ProizvodNaziv { get; set; } = null;
        public int ZahtevanaKolicina { get; set; } = 0;

        public string ToJsonString()
        {
            return JsonSerializer.Serialize<Obavestenje>(this);
        }
    }
}