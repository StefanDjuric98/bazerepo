import axios from "axios";
import authHeader from "./auth-header";

const API_URL = "https://localhost:5001/api/Prodavac/";


const getStatistikaTopProdavciKompanija=(br)=>{
    return axios.get(API_URL+"StatistikaTopProdavciKompanija/"+br, { headers: authHeader() });
}

export default{
    getStatistikaTopProdavciKompanija
}