
namespace BazaB.Models
{

    public class GetProdavnicaDTO
    {
        public int ID { get; set; }
        public string Ime { get; set; }
        public string Adresa { get; set; }
        public string Okrug { get; set; }
    }
}