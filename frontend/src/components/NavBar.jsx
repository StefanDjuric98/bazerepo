import React, { useState } from 'react';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import AuthService from '../services/auth.service'
import IconButton from '@material-ui/core/IconButton';
import Badge from '@material-ui/core/Badge';
import MailIcon from '@material-ui/icons/Mail';
import Avatar from '@material-ui/core/Avatar';
import Chip from '@material-ui/core/Chip';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import CancelIcon from '@material-ui/icons/Cancel';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import { lightGreen, red } from '@material-ui/core/colors'
import prodavnicaService from '../services/prodavnica.service';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
  link:{
      color: "white",
  }
}));

const StyledMenu = withStyles({
  paper: {
    border: '1px solid #d3d4d5',
  },
})((props) => (
  <Menu
    elevation={0}
    getContentAnchorEl={null}
    anchorOrigin={{
      vertical: 'bottom',
      horizontal: 'center',
    }}
    transformOrigin={{
      vertical: 'top',
      horizontal: 'center',
    }}
    {...props}
  />
));

const StyledMenuItem = withStyles((theme) => ({
  root: {
    '&:focus': {
      backgroundColor: theme.palette.secondary.main,
      '& .MuiListItemIcon-root, & .MuiListItemText-primary': {
        color: theme.palette.common.white,
      },
    },
  },
}))(MenuItem);

export default function NavBar({ user, obavestenja, setObavestenja }) {

  const classes = useStyles();
  const [anchorEl, setAnchorEl] = useState(null);
  const openObavestenja = Boolean(anchorEl);
  const [anchorElUser, setAnchorElUser] = React.useState(null);

  const isProdavac = user && user.role === "Prodavac";
  const isAdmin = user && user.role === "Admin";
  const isLogedIn = isProdavac || isAdmin;

  const logOut = () => {
    AuthService.logout();
  };

  const handleObavestenja = (ev) => {
    console.log(ev.currentTarget.clientTop);
    setAnchorEl(ev.currentTarget);
  }

  const handleCloseObavestenja = () => {
    setAnchorEl(null);
  }

  const handleActionNarudzbenica = (ev, obavestenje, prihvaceno) => {
    prodavnicaService.obradiNarudzbenicu(obavestenje, prihvaceno, user.family_name)
    .then(res => {
      console.log(res.data);
      const i = obavestenja.indexOf(obavestenje);
      let tmp = obavestenja;
      tmp.splice(i, 1);
      console.log(tmp);
      setObavestenja(tmp);
      tmp = anchorEl;
      setAnchorEl(null);
      setAnchorEl(tmp);
    })
    .catch(error => {
      console.log(error);
    });
  }

  const handleUkloni = (ev, obavestenje) => {
    prodavnicaService.ukloniObavestenje(obavestenje)
    .then(res => {
      console.log(res.data);
      const i = obavestenja.indexOf(obavestenje);
      let tmp = obavestenja;
      tmp.splice(i, 1);
      console.log(tmp);
      setObavestenja(tmp);
      tmp = anchorEl;
      setAnchorEl(null);
      setAnchorEl(tmp);
    })
    .catch(error => {
      console.log(error);
    });
  }

  const handleToggleUser = (event) => {
    setAnchorElUser(event.currentTarget);
  };

  const handleCloseUser = () => {
    setAnchorElUser(null);
  };

  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Toolbar>
          {/* <Button active="true" color="inherit" href="/"><HomeIcon />Home</Button> */}
          <Typography variant="h6" className={classes.title}>
            Supply Manager
          </Typography>
          {isAdmin && (
            <>
                <Button color="inherit" href="/KreirajOkrug">Novi okrug</Button>
                <Button color="inherit" href="/KreirajProdavnicu">Nova prodavnica</Button>
                <Button color="inherit" href="/KreirajProizvod">Novi proizvod</Button>
                <Button color="inherit" href="/DodajProdavca">Novi Prodavac</Button>
            </>  
          )}
          {isLogedIn ? (
            <>
              <Button color="inherit" href="/RangLista">Statistika</Button>
              <Button color="inherit" href={"/Prodaja/"+user.nameid}>Prodaja</Button>
              <Button color="inherit" href={"/DodajProizvod"}>Dodaj Proizvod</Button>
              <IconButton disabled={obavestenja.length === 0} onClick={handleObavestenja}>
                <Badge color="secondary" badgeContent={obavestenja.length} style={{ marginRight: 10}}>
                  <MailIcon />
                </Badge>
              </ IconButton>
              <Chip 
                variant="outlined" 
                color="secondary" 
                avatar={<Avatar alt={user.given_name} />} 
                label={user.given_name}
                clickable
                onClick={handleToggleUser}
                />
            </>
          ) : (
            <Button color="inherit" href="/Login">Login</Button>
          )}
          
        </Toolbar>
      </AppBar>

      <Menu
        id="obavestenjaMenu"
        anchorEl={anchorEl}
        keepMounted
        open={openObavestenja}
        onClose={handleCloseObavestenja}
        PaperProps={{
          style: {
            maxHeight: 40 * 4.5,
          },
        }}
      >
        {obavestenja.map((option, i) => (
          <MenuItem key={i} >
            <Typography variant="inherit" noWrap>
              {option.poruka}
            </Typography>
            {option.zahtev ? (
              <>
                <IconButton onClick={(ev) => handleActionNarudzbenica(ev, option, true)}><CheckCircleIcon style={{ color: lightGreen[500], marginLeft: 5}} /></IconButton>
                <IconButton onClick={(ev) => handleActionNarudzbenica(ev, option, false)}><CancelIcon style={{ color: red[500] }} /></IconButton>
              </>
            ):(
              <IconButton onClick={(ev) => handleUkloni(ev, option)}><CancelIcon style={{ color: red[500] }} /></IconButton>
            )}
          </MenuItem>
        ))}
      </Menu>

      <StyledMenu
        id="userMenu"
        anchorEl={anchorElUser}
        keepMounted
        open={Boolean(anchorElUser)}
        onClose={handleCloseUser}
      >
        <StyledMenuItem onClick={logOut} >
          <Button href="/Login">
            <ListItemIcon>
              <ExitToAppIcon fontSize="small" />
            </ListItemIcon>
            <ListItemText primary="Logout" />
          </Button>
        </StyledMenuItem>
      </StyledMenu>
    </div>
  );
}