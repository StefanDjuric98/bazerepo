using System.Collections.Generic;

namespace BazaB.Models
{
    public class ProdajaDTO
    {
        public int ProdavnicaId { get; set; }
        public int OkrugId { get; set; }
        public ProdavacDTO Prodavac { get; set; }
        public List<Stanje> ProizvodiZaProdaju { get; set; }
    }

}