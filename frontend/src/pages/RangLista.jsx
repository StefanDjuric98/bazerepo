import React, { useState, useEffect } from "react";
import { makeStyles } from '@material-ui/core/styles';
import serviceProdavnica from "../services/prodavnica.service";
import StatisitkaLista from "../components/StatistikaLista";
import serviceProizvod from "../services/proizvod.service";
import serviceProdavac from "../services/prodavac.service";
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import authService from "../services/auth.service";


const useStyles = makeStyles((theme) => ({
    form: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        flexWrap: 'nowrap',
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
      },
  }));

export default function RangLista() {
    const classes = useStyles();
    const [listaTopProfit, setListaTopProfit]=useState([]);
    const [topProizvodi, setTopProizvodi]=useState([]);
    const [topProdavci, setTopProdavci]=useState([]);
    const [prodavnice, setProdavnice]=useState([]);
    const [prodavnica, setProdavnica]=useState({ime: ""});
    const [statistikaProizvodi,setStatistikaProizvodi]=useState([]);
    const [statistikaProdavci, setStatistikaProdavci]=useState([]);
    const [currentUser, setCurrentUser] = useState(undefined);

    const isAdmin = currentUser && currentUser.role === "Admin";
   

    useEffect(()=>{
        const user = authService.getCurrentUser();

        if (user) {
            setCurrentUser(user);
        }

        serviceProdavnica.getStatistikaTopProfitProdavnice(6)
        .then(res=>{
            setListaTopProfit(res.data);
             console.log(res);
        })
        .catch(function (error)
        {
            console.log(error);
        })

        serviceProizvod.getStatistikaTopProizvodiKompanija(6)
        .then(res=>{
            setTopProizvodi(res.data);
        })
        .catch(function (error)
        {
            console.log(error);
        })

        serviceProdavac.getStatistikaTopProdavciKompanija(6)
        .then(res=>{
            let prodavci = res.data;
            prodavci.map((p, i) => {
                return prodavci[i] = {
                    data: p.data.ime + " (" + p.data.prodavnica + ")",
                    score: p.score
                }
            });
            setTopProdavci(prodavci);
            console.log(res.data);
        })
        .catch(function (error)
        {
            console.log(error);
        })
        serviceProdavnica.sveProdavnice()
        .then(res=>{
            var p=res.data;
            setProdavnice(p);
        })
        .catch(error => {
          console.log(error);
        });
    },[]);

    const handleChangeProdavnica=(ev)=>{
        setProdavnica(ev.target.value);
        console.log(ev.target.value.id);
        serviceProdavnica.getStatistikaProizvodiProdavnica(ev.target.value.id, 6)
            .then(res=>{
                setStatistikaProizvodi(res.data);
                console.log("statistika");
                console.log(res.data);
            })
            .catch(function (error)
            {
                console.log(error);
            });

            serviceProdavnica.getStatistikaProdavciProdavnica(ev.target.value.id, 6)
            .then(res=>{
                setStatistikaProdavci(res.data);
                console.log("statistika");
                console.log(res.data);
            })
            .catch(function (error)
            {
                console.log(error);
            });
      }

    return (
        <div className={classes.root}>
            <div className={classes.form}>
                <StatisitkaLista naslov={"Najbolje prodavnice"} lista={listaTopProfit}></StatisitkaLista>
                <StatisitkaLista naslov={"Najprodavaniji proizvodi"} lista={topProizvodi}></StatisitkaLista>
                <StatisitkaLista naslov={"Najbolji prodavci"} lista={topProdavci}></StatisitkaLista>
            </div>
            {isAdmin && (
            <div>
                    <FormControl variant="outlined" fullWidth className={classes.formControl}>
                        <InputLabel id="demo-simple-select-label">Izaberite prodavnicu</InputLabel>
                        <Select
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            //value={prodavnica}
                            // defaultValue={prodavnica}
                            onChange={(ev)=>handleChangeProdavnica(ev)}
                            label="Izaberite prodavnicu za prikaz najboljih proizvoda"
                            style={{ marginRight: 15 }}
                        >
                            {prodavnice.map((prodavnica,ind)=>
                            <MenuItem key={ind} value={prodavnica}>{prodavnica.ime}</MenuItem> )}
                            {/* {prodavnice.map((prodavnica, ind)=>
                                <MenuItem key={ind} value={prodavnica}>{prodavnica.ime}</MenuItem>
                                )
                            }         */}
                        </Select>
                    </FormControl>
                    <div className={classes.form}>
                        <StatisitkaLista naslov={"Najprodavaniji proizvodi u prodavnici :" + prodavnica.ime} lista={statistikaProizvodi}/>
                        <StatisitkaLista naslov={"Najbolji prodavci u prodavnici :"+prodavnica.ime} lista={statistikaProdavci}/>
                    </div>
            </div>
            )}
        </div>
        
    )
}
