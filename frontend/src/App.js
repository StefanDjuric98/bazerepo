import React, { useState, useEffect } from "react";
import prodavnica from "./components/prodavnica";
import proizvod from "./components/proizvod";
import dodajProizvode from "./components/dodajProizvod";
import kreirajOkrug from "./components/kreirajOkrug";
import dodajProdavca from "./components/dodajProdavca";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import axios from "axios";
import Login from "./pages/Login";
import NavBar from "./components/NavBar";
import authService from "./services/auth.service";
import EEditProizvod from "./components/editProizvod";
import EEditProdavnica from "./components/editProdavnica";
import rangLista from"./components/rangLista";
import prodaja from "./components/prodaja";
import prodavnicaService from "./services/prodavnica.service";

axios.defaults.baseURL = "https://localhost:5001/api/";

function App() {

  const [currentUser, setCurrentUser] = useState(undefined);
  const [obavestenja, setObavestenja] = useState([]);

  useEffect(() => {
    const user = authService.getCurrentUser();
    console.log(user);
    if (user) {
      setCurrentUser(user);

      prodavnicaService.uzmiObavestenja(user.groupsid)
      .then(res => {
        console.log(res.data);
        setObavestenja(res.data);
      })
      .catch(error => {
        console.log(error);
      });
    };

  }, []);

  return (
    <div className="App">
      <NavBar user={currentUser} obavestenja={obavestenja} setObavestenja={setObavestenja}/>
      <Router>
      <Switch>
        <Route path="/KreirajProdavnicu" component={prodavnica} />
        <Route path="/EditProdavnica/:prodavnicaId" component={EEditProdavnica} />
        <Route path="/KreirajProizvod" component={proizvod} />
        <Route path="/DodajProizvod" component={dodajProizvode}/>
        <Route path="/EditProizvod/:proizvodId" component={EEditProizvod} />
        <Route path="/KreirajOkrug" component={kreirajOkrug} />
        <Route path="/DodajProdavca" component={dodajProdavca} />
        <Route path="/Prodaja" component={prodaja} />
        <Route path="/RangLista" component={rangLista}/>
        <Route path="/" component={Login} />
      </Switch>
      </Router>
    </div>
  );
}

export default App;
