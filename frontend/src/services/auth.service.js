import axios from "axios";
import authHeader from "./auth-header";
import jwt from 'jwt-decode'

const API_URL = "https://localhost:5001/api/Prodavac/";

const register = (prodavac, password, prodavnicaId) => {
  return axios.post(API_URL + "KreirajProdavca", {
    prodavac,
    password,
    prodavnicaId
  }, { headers: authHeader() });
};

const login = (loginDTO) => {
  return axios.post(API_URL + "Login", loginDTO)
    .then((response) => {
      if (response.statusText === "OK") {
        localStorage.setItem("user", JSON.stringify(response.data));
      }

      return response.data;
    });
};

const logout = () => {
  localStorage.removeItem("user");
};

const getCurrentUser = () => {
  var token = JSON.parse(localStorage.getItem("user"));
  if(token)
    return jwt(token);
  return undefined;
};

export default {
  register,
  login,
  logout,
  getCurrentUser,
};
