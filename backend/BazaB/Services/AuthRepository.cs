using System.Threading.Tasks;
using BazaB.Models;
using Neo4j.Driver;
using System.Security.Cryptography;
using System.Text;
using System.Collections.Generic;
using System.Security.Claims;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;

namespace BazaB.Services
{
    public class AuthRepository : IAuthRepository
    {
        private readonly IDriver _driver;
        private readonly IConfiguration _configuration;

        public AuthRepository(IDriver driver, IConfiguration configuration)
        {
            _driver = driver;

            _configuration = configuration;
        }

        public async Task<string> Login(string username, string password)
        {
            if(!(await UserExists(username)))
                return null;

            Prodavac prodavac;
            int prodavnicaId = -1;
            string prodavnicaIme = String.Empty;
            var session = _driver.AsyncSession();
            try
            {
                prodavac = await session.ReadTransactionAsync(async tx =>
                {
                    var result = await tx.RunAsync("MATCH (p:Prodavac { username: '" + username + "' })" +
                                                    " OPTIONAL MATCH (p)-[:RADI_U]->(prod:Prodavnica) RETURN p, prod.id as pid, prod.ime as pime");
                    return await result.SingleAsync(res =>
                    {
                        if(res["pid"] != null)
                        {
                            prodavnicaId = res["pid"].As<int>();
                            prodavnicaIme = res["pime"].As<string>();
                        }
                        var node = res["p"].As<INode>();
                        return new Prodavac()
                        {
                            ID = Convert.ToInt16(node.Properties["id"]?.As<int>()),
                            Role = node.Properties["role"].As<string>(),
                            Username = node.Properties["username"].As<string>(),
                            Ime = node.Properties["name"].As<string>(),
                            Prezime = node.Properties["prezime"].As<string>(),
                            PasswordHash = node.Properties["passwordHash"].As<byte[]>(),
                            PasswordSalt = node.Properties["passwordSalt"].As<byte[]>()
                        };
                    });
                });
            }
            finally
            {
                await session.CloseAsync();
            }

            if(!VerifyPasswordHash(password, prodavac.PasswordHash, prodavac.PasswordSalt))
                return null;
            
            return CreateToken(prodavac, prodavnicaId, prodavnicaIme);
        }

        public async Task<int?> Register(Prodavac prodavac, string password)
        {
            if(await UserExists(prodavac.Username))
                return null;
            
            CreatePasswordHash(password, out byte[] passwordHash, out byte[] passwordSalt);

            int id = NextIdHelper.GetId(_driver).Result;
        
            string statementText = "CREATE (p:Prodavac {id: $id, name: $ime, prezime: $prezime, role: $role, " + 
                                    "username: $username, passwordHash: $passwordHash, passwordSalt: $passwordSalt})";
    
            var statementParameters = new Dictionary<string, Object>
            {
                {"id", id},
                {"ime", prodavac.Ime },
                {"prezime", prodavac.Prezime},
                {"role", prodavac.Role},
                {"username", prodavac.Username},
                {"passwordHash", passwordHash},
                {"passwordSalt", passwordSalt}
            };

            var session = _driver.AsyncSession();
            try 
            {
                await session.WriteTransactionAsync(tx => tx.RunAsync(statementText.ToString(), statementParameters));
            }
            finally
            {
                await session.CloseAsync();
            }
            
            return id;
        }

        public async Task<bool> UserExists(string username)
        {
            var session = _driver.AsyncSession();
            try
            {
                bool prodavacExists = await session.ReadTransactionAsync(async tx =>
                {
                    var result = await tx.RunAsync("MATCH (p:Prodavac { username: '" + username + "' }) RETURN count(*) as count");
                
                    return await result.SingleAsync(res =>
                    {
                        return res["count"].As<int>() > 0;
                    });
                });

                return prodavacExists;
            }
            finally
            {
                await session.CloseAsync();
            }
        }

        private void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            using(var hmac = new HMACSHA512())
            {
                passwordSalt = hmac.Key;
                passwordHash = hmac.ComputeHash(Encoding.UTF8.GetBytes(password));
            }
        }

        private bool VerifyPasswordHash(string password, byte[] passwordHash, byte[] passwordSalt)
        {
            using(var hmac = new HMACSHA512(passwordSalt))
            {
                byte[] computedHash = hmac.ComputeHash(Encoding.UTF8.GetBytes(password));
                for(int i = 0; i < computedHash.Length; i++)
                    if(computedHash[i] != passwordHash[i])
                        return false;
                return true;
            }
        }

        private string CreateToken(Prodavac prodavac, int prodavnicaId, string prodavnicaIme)
        {
            List<Claim> claims = new List<Claim>
            {
                new Claim(ClaimTypes.NameIdentifier, prodavac.ID.ToString()),
                new Claim(ClaimTypes.Name, prodavac.Username),
                new Claim(ClaimTypes.GivenName, prodavac.Ime + " " + prodavac.Prezime),
                new Claim(ClaimTypes.Role, prodavac.Role),
                new Claim(ClaimTypes.GroupSid, prodavnicaId.ToString()),
                new Claim(ClaimTypes.Surname, prodavnicaIme)
            };

            SymmetricSecurityKey key = new SymmetricSecurityKey(Encoding.UTF8
                                                        .GetBytes(_configuration.GetSection("AppSettings:Token").Value));

            SigningCredentials creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha512Signature);

            SecurityTokenDescriptor tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims),
                Expires = DateTime.Now.AddHours(6),
                SigningCredentials = creds
            };

            JwtSecurityTokenHandler tokenHandler = new JwtSecurityTokenHandler();
            SecurityToken token = tokenHandler.CreateToken(tokenDescriptor);

            return tokenHandler.WriteToken(token);
        }

    }

    public interface IAuthRepository
    {
        Task<int?> Register(Prodavac prodavac, string password);
        Task<string> Login(string username, string password);
        Task<bool> UserExists(string username);

    }
}