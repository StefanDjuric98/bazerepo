using System.Threading.Tasks;
using BazaB.Models;
using Neo4j.Driver;
using System.Security.Cryptography;
using System.Text;
using System.Collections.Generic;
using System.Security.Claims;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using StackExchange.Redis;
using System.Text.Json;

public class RedisService
{
    private readonly string _redisHost;
    private readonly int _redisPort;
    private ConnectionMultiplexer _redis;
    public RedisService(IConfiguration config)
    {
        _redisHost = config["Redis:Host"];
        _redisPort = Convert.ToInt32(config["Redis:Port"]);
    }

    public void Connect()
    {
        try
        {
            var configString = $"{_redisHost}:{_redisPort},connectRetry=5";
            _redis = ConnectionMultiplexer.Connect(configString);
        }
        catch (RedisConnectionException err)
        {
            // Log.Error(err.ToString());
            Console.WriteLine(err.ToString());
            // throw err;
        }
        Console.WriteLine("Connected to Redis");
    }

    public async Task Set(string key, string value)
    {
        var result = await _redis.GetDatabase().StringSetAsync(key, value);
        await _redis.GetDatabase().SortedSetIncrementAsync("sortirani:proba", "GeForce RTX 3060", 3);
    }

    public async Task<string> Get(string key)
    {
        var db = _redis.GetDatabase();
        return await db.StringGetAsync(key);
    }

    public async Task UpdateStatistika(ProdajaDTO prodaja)
    {
        var db = _redis.GetDatabase();
        int ukupnaCena = 0;
        foreach(Stanje p in prodaja.ProizvodiZaProdaju)
        {
            await db.SortedSetIncrementAsync("proizvodi:" + prodaja.ProdavnicaId + ":prodavnica", p.Proizvod.Naziv, p.Kolicina);
            await db.SortedSetIncrementAsync("proizvodi:kompanija", p.Proizvod.Naziv, p.Kolicina);
            ukupnaCena += p.Kolicina * p.Proizvod.Cena;
        }
        await db.SortedSetIncrementAsync("prodavci:" + prodaja.ProdavnicaId + ":prodavnica", prodaja.Prodavac.Ime, ukupnaCena);
        await db.SortedSetIncrementAsync("prodavci:kompanija", prodaja.Prodavac.ToJsonString(), ukupnaCena);
        await db.SortedSetIncrementAsync("top:prodavnice", prodaja.Prodavac.Prodavnica, ukupnaCena);
        
    }

    public async Task<SortedSetEntry[]> GetSortedSet(int count)
    {
        var db = _redis.GetDatabase();
        return await db.SortedSetRangeByRankWithScoresAsync("sortirani:proba", 0, count, Order.Descending);
    }

    public async Task<List<Statistika<string>>> GetStatistikaDesc(string key, int count)
    {
        var db = _redis.GetDatabase();
        var niz = await db.SortedSetRangeByRankWithScoresAsync(key, 0, count - 1, Order.Descending);
        List<Statistika<string>> statistika = new List<Statistika<string>>();
        foreach(var r in niz)
        {
            var record = new Statistika<string>()
            {
                Data = r.Element.ToString(),
                Score = r.Score
            };
            statistika.Add(record);
        }

        return statistika;
    }

    public async Task<List<Statistika<ProdavacDTO>>> GetStatistikaProdavciKompanijaDesc(int count)
    {
        var db = _redis.GetDatabase();
        var niz = await db.SortedSetRangeByRankWithScoresAsync("prodavci:kompanija", 0, count - 1, Order.Descending);
        List<Statistika<ProdavacDTO>> statistika = new List<Statistika<ProdavacDTO>>();
        foreach(var r in niz)
        {
            var record = new Statistika<ProdavacDTO>()
            {
                Data = (ProdavacDTO)JsonSerializer.Deserialize(r.Element, typeof(ProdavacDTO)),
                Score = r.Score
            };
            statistika.Add(record);
        }

        return statistika;
    }

    public async Task DeleteProdavac(int prodavnicaId, ProdavacDTO prodavac)
    {
        var db = _redis.GetDatabase();
        await db.SortedSetRemoveAsync("prodavci:" + prodavnicaId + ":prodavnica", prodavac.Ime);
        await db.SortedSetRemoveAsync("prodavci:kompanija", prodavac.ToJsonString());
    }

    public async Task DeleteProizvod(List<int> prodavnice, string proizvod)
    {
        var db = _redis.GetDatabase();
        foreach(int id in prodavnice)
            await db.SortedSetRemoveAsync("proizvodi:" + id + ":prodavnica", proizvod);
        await db.SortedSetRemoveAsync("proizvodi:kompanija", proizvod);
    }

    public async Task DeleteProdavnica(Prodavnica prodavnica, List<ProdavacDTO> prodavci)
    {
        var db = _redis.GetDatabase();
        db.KeyDelete("prodavci:" + prodavnica.ID + ":prodavnica");
        db.KeyDelete("proizvodi:" + prodavnica.ID + ":prodavnica");
        db.KeyDelete("obavestenja:" + prodavnica.ID + ":prodavnica");
        await db.SortedSetRemoveAsync("top:prodavnice", prodavnica.Ime);
        foreach(var p in prodavci)
            await db.SortedSetRemoveAsync("prodavci:kompanija", p.ToJsonString());
    }

    public async Task DeleteOkrug(List<Prodavnica> prodavnice, List<List<ProdavacDTO>> prodavci)
    {
        for(int i = 0; i<prodavnice.Count; i++)
            await DeleteProdavnica(prodavnice[i], prodavci[i]);
    }

    public async Task PostObavestenje(Obavestenje obavestenje)
    {
        var db = _redis.GetDatabase();
        await db.ListLeftPushAsync("obavestenja:" + obavestenje.ZaId + ":prodavnica",  obavestenje.ToJsonString());
    }

    public async Task<List<Obavestenje>> GetObavestenja(int id)
    {
        var db = _redis.GetDatabase();
        var results = await db.ListRangeAsync("obavestenja:" + id + ":prodavnica",  0, -1);
        List<Obavestenje> obavestenja = new List<Obavestenje>();
        foreach(var r in results)
            obavestenja.Add((Obavestenje)JsonSerializer.Deserialize(r, typeof(Obavestenje)));

        return obavestenja;
    }

    public async Task<long> DeleteObavestenje(Obavestenje obavestenje)
    {
        var db = _redis.GetDatabase();
        long res = await db.ListRemoveAsync("obavestenja:" + obavestenje.ZaId + ":prodavnica",  obavestenje.ToJsonString(), -1);
        return res;
    }
}