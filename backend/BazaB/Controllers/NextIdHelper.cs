using System.Threading.Tasks;
using Neo4j.Driver;

public class NextIdHelper
{
    public static async Task<int> GetId(IDriver _driver)
    {
        var session = _driver.AsyncSession();

        try
        {
            int maxId;
            return await session.ReadTransactionAsync(async tx =>
            {
                var result = await tx.RunAsync("MATCH (n) WHERE exists(n.id) RETURN max(n.id) as id");

                maxId = await result.SingleAsync(res => res["id"].As<int>());
                return ++maxId;

            });

        }
        finally
        {
            await session.CloseAsync();
        }
    }
}
