import React, { useState } from 'react'
import { useHistory } from "react-router-dom";

import Button from '@material-ui/core/Button';
import { ValidatorForm, TextValidator} from 'react-material-ui-form-validator';
import AuthService from "../services/auth.service";
import Container from "@material-ui/core/Container";
import Avatar from "@material-ui/core/Avatar";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import IconButton from "@material-ui/core/IconButton";
import VisibilityIcon from "@material-ui/icons/Visibility";
import VisibilityOffIcon from "@material-ui/icons/VisibilityOff";
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const useStyles = makeStyles((theme) => ({
    paper: {
      marginTop: theme.spacing(8),
      display: "flex",
      flexDirection: "column",
      alignItems: "center",
    },
    avatar: {
      margin: theme.spacing(1),
      backgroundColor: theme.palette.secondary.main,
    },
    form: {
      width: "100%", // Fix IE 11 issue.
      marginTop: theme.spacing(1),
    },
    submit: {
      margin: theme.spacing(3, 0, 2),
    },
    button: {
      marginLeft: "86%",
      marginTop: "-24%",
      backgroundColor: "transparent",
    },
    input: {
      backgroundColor: "transparent",
      //border: 2px solid black;
    },
    root: {
      width: '100%',
      '& > * + *': {
        marginTop: theme.spacing(2),
      },
    },
  }));

export default function Login() {

    let history = useHistory();
    const classes = useStyles();
    const [loginDTO, setLoginDTO]=useState({
        username: "",
        password: ""
    });
    const [showPassword, setShowPassword] = useState(false);
    const [message, setMessage] = React.useState({open: false, content: ""});

    const handleChange = (ev) => {

        setLoginDTO({ ...loginDTO, [ev.target.name]: ev.target.value });
    }

    const handleClickShowPassword = () => {
      if (showPassword === false) {
        setShowPassword(true);
      } else setShowPassword(false);
    };

    const handleClose = (event, reason) => {
      if (reason === 'clickaway') {
        return;
      }
  
      setMessage({ ...message, open: false });
    };

    const handleSubmit=(ev)=>{
        ev.preventDefault();
        AuthService.login(loginDTO).then(
         () => {
            const user = AuthService.getCurrentUser();
            if(user.role === "Prodavac")
              history.push("/Prodaja/"+2);
            else
              history.push("/DodajProizvod");
            window.location.reload();
          })
          .catch(error =>
          {
            console.log(error);
            setMessage({ open: true, content: [error.response.data]});
          });
    }

    return(
        <div>
            <Container component="main" maxWidth="xs">
                <div className={classes.paper}>
                
                    <Avatar className={classes.avatar}></Avatar>
                    <Typography component="h1" variant="h5">
                        Sign in
                    </Typography>

                    <ValidatorForm
                    className={classes.form}
                        onSubmit={handleSubmit}
                        onError={errors => console.log(errors)}
                    >
                            <TextValidator
                                label="Username"
                                variant="outlined"
                                fullWidth
                                margin="normal"
                                onChange={handleChange}
                                name="username"
                                value={loginDTO.username}
                                validators={['required']}
                                errorMessages={['Korisničko ime je obavezno']}
                            />
                            <div>
                              <TextValidator
                                  label="Password"
                                  variant="outlined"
                                  fullWidth
                                  margin="normal"
                                  onChange={handleChange}
                                  name="password"
                                  type={showPassword ? "text" : "password"}
                                  value={loginDTO.password}
                                  validators={['required']}
                                  errorMessages={['Šifra je obavezna']}
                              />
                              <IconButton
                                aria-label="toggle password visibility"
                                dge="end"
                                className={classes.button}
                                onClick={handleClickShowPassword}
                              >
                                {showPassword ? <VisibilityIcon /> : <VisibilityOffIcon />}
                              </IconButton>
                            </div>
                            
                            <Button
                                type="submit"
                                fullWidth
                                variant="contained"
                                color="primary"
                            >
                                Login
                            </Button>
                    </ValidatorForm>
            
                </div>
            </Container>

            <div className={classes.root}>
                    <Snackbar open={message.open} autoHideDuration={6000} onClose={handleClose}>
                        <Alert onClose={handleClose} severity="error">
                            {message.content}
                        </Alert>
                    </Snackbar>
            </div>
        </div>
    );
}