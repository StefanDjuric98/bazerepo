using System;

namespace BazaB.Models
{

    public class Stanje
    {
        public int Kolicina { get; set; }
        public int MinKolicina { get; set; }
        public Prodavnica Prodavnica { get; set; }
        public Proizvod Proizvod { get; set; }
    }
}