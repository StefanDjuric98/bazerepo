namespace BazaB.Models
{
    public class CheckStanjeDTO
    {
        public Proizvod Proizvod { get; set; }
        public int ProdavnicaId { get; set; }
        public int OkrugId { get; set; }
    }
}