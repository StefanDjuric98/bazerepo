import axios from "axios";
import authHeader from "./auth-header";

const API_URL = "https://localhost:5001/api/Proizvod/";

const kreirajProizvod = (proizvod) => {
    return axios.post(API_URL + "KreirajProizvod", proizvod, { headers: authHeader() });
}

const getProizvodi = () => {
    return axios.get(API_URL + "SviProizvodi", { headers: authHeader() });
}

const uzmiProizvod = (id) => {
    return axios.get(API_URL + "UzmiProizvod/" + id, { headers: authHeader() });
}

const izmeniProizvod = (proizvod) => {
    return axios.put(API_URL + "IzmeniProizvod", proizvod, { headers: authHeader() });
}

const obrisiProizvod = (id) => {
    return axios.delete(API_URL + "ObrisiProizvod/" + id, { headers: authHeader() });
}

const getStatistikaTopProizvodiKompanija=(br)=>{
    return axios.get(API_URL+"StatistikaTopProizvodiKompanija/"+br, { headers: authHeader() });
}

export default{
    kreirajProizvod,
    getProizvodi,
    uzmiProizvod,
    izmeniProizvod,
    obrisiProizvod,
    getStatistikaTopProizvodiKompanija
};