import React from 'react'
import { useParams } from "react-router-dom";
import EditProizvod from '../pages/EditProizvod';


export default function EEditProizvod() {
    const { proizvodId } = useParams();
    return (
        <div>
            <EditProizvod id={proizvodId}/>
        </div>
    )
}