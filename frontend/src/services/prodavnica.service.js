import axios from "axios";
import authHeader from "./auth-header";

const API_URL="https://localhost:5001/api/Prodavnica/";

const kreirajProdavnicu=(prodavnica, okrug)=>{
    return axios.post(API_URL + "KreirajProdavnicu", {
        prodavnica,
        okrugId: okrug
      }, { headers: authHeader() });
};

const kreirajLinkProizvod=(data)=>{
    return axios.post(API_URL+"KreirajLinkProizvod", data, { headers: authHeader() });
};

const uzmiJednuProdavnicu = (id) => {
    return axios.get(API_URL+"UzmiProdavnicu/" + id, { headers: authHeader() });
}

const sveProdavnice=()=>{
    return axios.get(API_URL+"SveProdavnice", { headers: authHeader() });
};

const izmeniProdavnicu = (prodavnica) => {
    return axios.put(API_URL + "IzmeniProdavnicu", prodavnica, { headers: authHeader() });
};

const nadjiLinkProdavnicaPorizvod=(idProizvod,idProdavnica)=>{
    return axios.get(API_URL+"NadjiProizvod/"+idProizvod+"/"+idProdavnica, { headers: authHeader() });
}

const updateLinkProdavnicaProizovd=(data,idProdavnica,idProizvod)=>{
    return axios.put(API_URL+"UpdateLinkProizovdProdavnica/"+idProizvod+"/"+idProdavnica, data, { headers: authHeader() });
}

const nadjiProdavnicuSaOdredjenimProizvodom = (proizvod, prodavnicaId, okrugId)=>{
    return axios.post(API_URL + "NadjiProdavnicuSaOdredjenimProizvodom", {
        proizvod,
        prodavnicaId,
        okrugId
      }, { headers: authHeader() });
}

const izvrsiProdaju = (prodavnicaId, okrugId, prodavacIme, prodavnicaIme, proizvodiZaProdaju)=>{
    return axios.put(API_URL + "IzvrsiProdaju", {
        prodavnicaId,
        okrugId,
        prodavac: {
            ime: prodavacIme,
            prodavnica: prodavnicaIme
        },
        proizvodiZaProdaju
      }, { headers: authHeader() });
}

const obrisiProdavnicu=(id)=>{
    return axios.delete(API_URL + "ObrisiProdavnicu/" + id, { headers: authHeader() });
};

const getStatistikaProizvodiProdavnica=(id,br)=>{
    return axios.get(API_URL+"StatistikaProizvodiProdavnica/"+id+"/"+br, { headers: authHeader() });
};

const getStatistikaProdavciProdavnica=(id, br)=>{
    return axios.get(API_URL+"StatistikaProdavciProdavnica/"+id+"/"+br, { headers: authHeader() });
}

const getStatistikaTopProfitProdavnice=(br)=>{
    return axios.get(API_URL+"StatistikaTopProfitProdavnice/"+br, { headers: authHeader() });
}

const posaljiNarudzbenicu = (data, prodavnica) => {
    return axios.post(API_URL + "PosaljiNarudzbenicu/" + prodavnica, data, { headers: authHeader() });
};

const uzmiObavestenja = (id) => {
    return axios.get(API_URL + "UzmiObavestenja/" + id, { headers: authHeader() });
};

const obradiNarudzbenicu = (data, prihvaceno, prodavnica) => {
    return axios.put(API_URL + "ObradiNarudzbenicu/" + prodavnica +"/" + prihvaceno, data, { headers: authHeader() });
};

const ukloniObavestenje = (data) => {
    return axios.put(API_URL + "UkloniObavestenje", data, { headers: authHeader() });
};

export default{
    kreirajProdavnicu,
    kreirajLinkProizvod,
    uzmiJednuProdavnicu,
    sveProdavnice,
    izmeniProdavnicu,
    nadjiLinkProdavnicaPorizvod,
    updateLinkProdavnicaProizovd,
    nadjiProdavnicuSaOdredjenimProizvodom,
    izvrsiProdaju,
    obrisiProdavnicu,
    getStatistikaProizvodiProdavnica,
    getStatistikaProdavciProdavnica,
    getStatistikaTopProfitProdavnice,
    posaljiNarudzbenicu,
    uzmiObavestenja,
    obradiNarudzbenicu,
    ukloniObavestenje
};