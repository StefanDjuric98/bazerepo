using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Neo4j.Driver;
using System.Text;
using BazaB.Models;
using System.Net.Http;
using Microsoft.AspNetCore.Authorization;
using BazaB.Services;
using StackExchange.Redis;

[Route("api/[controller]")]
[ApiController]
public class ProizvodController : ControllerBase
{
    private readonly IDriver _driver;

    private readonly RedisService _redisService;

    public ProizvodController(IDriver driver, RedisService redisService)
    {
        _driver = driver;
        _redisService=redisService;
    }

    [Authorize(Roles = "Admin")]
    [HttpPost]
    [Route("KreirajProizvod")]
    public async Task<IActionResult> CreateProizvod(Proizvod proizvod)
    {
        var session = this._driver.AsyncSession();
        try
        {
            bool proizvodExists = await session.ReadTransactionAsync(async tx =>
            {
                var result = await tx.RunAsync("MATCH (p:Proizvod { naziv: '" + proizvod.Naziv + "' }) RETURN count(*) as count");
            
                return await result.SingleAsync(res =>
                {
                    return res["count"].As<int>() > 0;
                });
            });

            if(proizvodExists)
                return BadRequest("Proizvod: " + proizvod.Naziv + " already exists in the database!");
        
            proizvod.ID = await NextIdHelper.GetId(_driver);
            string statementText = "CREATE ( n:Proizvod {id: $id, naziv: $naziv, kolicina: $kolicina, vrsta: $vrsta, cena: $cena})";
            var statementParameters = new Dictionary<string, Object>
            {
                {"id", proizvod.ID},
                {"naziv", proizvod.Naziv },
                {"kolicina", proizvod.Kolicina},
                {"vrsta", proizvod.Vrsta},
                {"cena", proizvod.Cena}
            };

            var result = await session.WriteTransactionAsync(tx => tx.RunAsync(statementText, statementParameters));
        }
        finally
        {
            await session.CloseAsync();
        }
        
        return Ok("Node Proizvod id: " + proizvod.ID + " has been created in the database");
    }

    [Authorize(Roles = "Admin,Prodavac")]
    [HttpGet]
    [Route("SviProizvodi")]
    public async Task<List<Proizvod>> GetProizvode()
    {
        var session = _driver.AsyncSession();
        try
        {
            var list = new List<Proizvod>();
            return await session.ReadTransactionAsync(async tx =>
            {
                var result = await tx.RunAsync("MATCH (p:Proizvod) RETURN p ORDER BY p.vrsta");

                await result.ForEachAsync(res =>
                {
                    var node = res["p"].As<INode>();
                    list.Add(new Proizvod()
                    {
                        ID = node.Properties["id"].As<int>(),
                        // ID = node.Properties["id"].As<int>(),
                        Naziv = node.Properties["naziv"].As<string>(),
                        Vrsta = node.Properties["vrsta"].As<string>(),
                        Cena = node.Properties["cena"].As<int>(),
                        Kolicina = node.Properties["kolicina"].As<int>()
                    });
                });
                return list;
            });
        }
        finally
        {
            await session.CloseAsync();
        }
    }

    [Authorize(Roles = "Admin, Prodavac")]
    [HttpGet]
    [Route("UzmiProizvod/{id}")]
    public async Task<Proizvod> GetProizvod(int id)
    {
        var session = _driver.AsyncSession();
        try
        {
            return await session.ReadTransactionAsync(async tx =>
            {
                Proizvod proizvod;
                var result = await tx.RunAsync("MATCH (p:Proizvod { id:" + id + " }) RETURN p");

                proizvod = await result.SingleAsync(res =>
                {
                    var node = res["p"].As<INode>();
                    return new Proizvod()
                    {
                        ID = Convert.ToInt16(node.Properties["id"]?.As<int>()),
                        Naziv = node.Properties["naziv"].As<string>(),
                        Kolicina = node.Properties["kolicina"].As<int>(),
                        Vrsta = node.Properties["vrsta"].As<string>(),
                        Cena = node.Properties["cena"].As<int>()
                    };
                });
                return proizvod;
            });
        }
        finally
        {
            await session.CloseAsync();
        }
    }

    [Authorize(Roles = "Admin")]
    [HttpPut]
    [Route("IzmeniProizvod")]
    public async Task<IActionResult> EditProizvod(Proizvod proizvod)
    {
        var session = _driver.AsyncSession();
        try
        {
            var statementParameters = new Dictionary<string, Object>
            {
                {"id", proizvod.ID},
                {"naziv", proizvod.Naziv},
                {"kolicina", proizvod.Kolicina},
                {"vrsta", proizvod.Vrsta},
                {"cena", proizvod.Cena}
            };
            string statementText = "MATCH (p:Proizvod {id: $id}) " +
                        "SET p.naziv = $naziv, p.kolicina = $kolicina, p.vrsta = $vrsta, p.cena = $cena";

            await session.WriteTransactionAsync(tx=>tx.RunAsync(statementText, statementParameters));
            return Ok("Proizvod has been updated!");
        }
        finally
        {
            await session.CloseAsync();
        }
    }

    [Authorize(Roles = "Admin")]
    [HttpDelete]
    [Route("ObrisiProizvod/{id}")]
    public async Task<IActionResult> DeleteProizvod(int id)
    {
        var session = _driver.AsyncSession();
        try
        {
            List<int> prodavnice = new List<int>();
            string proizvod = String.Empty;
            await session.ReadTransactionAsync(async tx =>
                {
                    var result = await tx.RunAsync("MATCH (p:Proizvod { id:" + id + " })<-[:IMA]-(prod:Prodavnica) " +
                                                    "RETURN p.naziv as naziv, prod.id as prodavnica");
                    return await result.ForEachAsync(res =>
                    {
                        if(proizvod == String.Empty)
                            proizvod = res["naziv"].As<string>();

                        prodavnice.Add(Convert.ToInt16(res["prodavnica"].As<int>()));
                    });
                });
            string deleteProizvod = "MATCH (p:Proizvod { id:" + id + " }) DETACH DELETE p";

            var result = await session.WriteTransactionAsync(tx => tx.RunAsync(deleteProizvod));

            await _redisService.DeleteProizvod(prodavnice, proizvod);
            
            return StatusCode(200, "Obrisan Proizvod id:" + id);
        }
        finally
        {
            await session.CloseAsync();
        }
    }

    [Authorize(Roles = "Admin, Prodavac")]
    [HttpGet]
    [Route("StatistikaTopProizvodiKompanija/{br}")]
    public async Task<IActionResult> GetStatistikaProizvodiKompanija(int br)
    {
        string key = "proizvodi:kompanija";
        var statistika = await _redisService.GetStatistikaDesc(key, br);
        return Ok(statistika);
    }
}