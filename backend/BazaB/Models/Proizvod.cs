using System;

namespace BazaB.Models
{

    public class Proizvod
    {
        public int ID { get; set; }
        public string Naziv { get; set; }
        public int Kolicina { get; set; }
        public string Vrsta { get; set; }
        public int Cena { get; set; }
    }
}