import React from 'react'
import Prodaja from '../pages/Prodaja';
import { useParams } from "react-router-dom";

export default function Pprodaja() {
    const{userId}=useParams();
    return (
        <div>
            <Prodaja id={userId}/>
        </div>
    )
}
