using System.Collections.Generic;

namespace BazaB.Models
{

    public class OkrugDTO
    {
        public Okrug okrug { get; set; }

        public List<Udaljenost> veze { get; set; }
    }
}