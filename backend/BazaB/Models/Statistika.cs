
namespace BazaB.Models
{
    public class Statistika<T>
    {
        public T Data { get; set; }
        public double Score { get; set; } 
    }
}