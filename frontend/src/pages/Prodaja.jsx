import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";

//
import proizvodService from "../services/proizvod.service";
import prodavnicaService from"../services/prodavnica.service";
import okrugService from"../services/okrug.service";

import { makeStyles } from "@material-ui/core/styles";
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import HomeWorkIcon from '@material-ui/icons/HomeWork';
import LocationCityIcon from '@material-ui/icons/LocationCity';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import authService from "../services/auth.service";
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import StatisitkaLista from "../components/StatistikaLista";
import SendIcon from '@material-ui/icons/Send';
import CloseIcon from '@material-ui/icons/Close';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const useStyles=makeStyles((theme)=>({
    demo: {
        backgroundColor: theme.palette.background.paper,
    },
    title: {
    margin: theme.spacing(4, 0, 2),
    },
    
    naslov: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        flexWrap: 'nowrap',
    },
    form: {
        width: "100%", 
        marginTop: theme.spacing(1),
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
      },
    selectEmpty: {
        marginTop: theme.spacing(2),
    },
    button: {
        marginLeft: "88%",
        marginTop: "-27%",
        backgroundColor: "transparent",
    },
    check:{
        color:"green",
    }
}))

export default function Prodaja(props) {
    let history = useHistory();
    const classes = useStyles();
    const [prodavnica, setProdavnica] = useState({});
    const [prodavnice, setProdavnice] = useState([]);
    const [proizvodi, setProizvodi] = useState([{}]);
    const [kolicina, setKolicina] = useState([]);
    const [okrug, setOkrug] = useState({ime: ""});
    const [currentUser, setCurrentUser] = useState(undefined);
    const [vidljiv, setVidljiv] = useState([]);
    const [proizvodiZaProdaju, setProizvodiZaProdaju] = useState([]);
    const [ukupnaCena, setUkupnaCena] = useState(0);
    const [message, setMessage] = useState({open: false, type: "info", request: false, content: "", reload: false});
    const [dialog, setDialog] = useState({open: false, content: "", kolicina: 0, count: 0, prodaja: false});
    const [proizvodiZaPorucivanje, setProizvodiZaPorucivanje] = useState([])
    const [statistikaProdavci, setStatistikaProdavci] = useState([]);
    const [statistikaProizvodi, setStatistikaProizvodi] = useState([]);

    const isProdavac = currentUser && currentUser.role === "Prodavac";
    const isAdmin = currentUser && currentUser.role === "Admin";
    const isAdminOrProdavac = isProdavac || isAdmin;
    const isUsersProdavnica = currentUser && parseInt(currentUser.groupsid) === prodavnica.id;

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
          return;
        }
    
        setMessage({ ...message, request: false, open: false });
        if(message.reload)
            window.location.reload();
    };

    const handleNaruci = () => {
        console.log(proizvodiZaPorucivanje);
        setMessage({ ...message, reload: false})
        setDialog({ ...dialog, open: true, content: proizvodiZaPorucivanje[0].poruka })
    };

    const handleChangeDialogKolicina = (ev) => {
        setDialog({ ...dialog, kolicina: ev.target.value });
    };

    const handleCloseDialog = () => {
        let count = dialog.count + 1;
        if(count < proizvodiZaPorucivanje.length)
            setDialog({ ...dialog, open: true, content: proizvodiZaPorucivanje[count].poruka, kolicina: 0, count: count })
        else {
            setDialog({ open: false, content: "", kolicina: 0, count: 0 });
            if(dialog.prodaja)
                window.location.reload();
        }
    };

    const handlePosaljiZahtev = () => {
        if(dialog.kolicina > proizvodiZaPorucivanje[dialog.count].dostupnaKolicina)
        {
            setMessage({open: true, type: "error", request: false, content: "Uneta količina je veća od količine koju je moguće naručiti!", reload: false});
            return;
        }
        console.log(dialog);

        const obavestenje = {
            zahtev: true,
            odId: prodavnica.id,
            zaId: proizvodiZaPorucivanje[dialog.count].prodavnica.id,
            proizvodId: proizvodiZaPorucivanje[dialog.count].proizvodId,
            proizvodNaziv: proizvodiZaPorucivanje[dialog.count].proizvodNaziv,
            zahtevanaKolicina: dialog.kolicina
        }
        prodavnicaService.posaljiNarudzbenicu(obavestenje, prodavnica.ime)
        .then(res => {
            console.log(res.data);
        })
        .catch(error => {
          console.log(error);
        });

        handleCloseDialog();
    };

    useEffect(() => {

        const user = authService.getCurrentUser();

        if (user) {
            setCurrentUser(user);

            if(parseInt(user.groupsid) !== -1)
            {
                prodavnicaService.uzmiJednuProdavnicu(user.groupsid)
                .then(res=>{
                    var p=res.data;
                    setProdavnica(p); 
                })
                .catch(function (error)
                {
                    console.log(error);
                });

                okrugService.uzmiOkrug(user.groupsid)
                .then(res=>{
                    setOkrug(res.data);
                    console.log("ovako izgleda okrug"+res.data);
                })
                .catch(function (error)
                {
                    console.log(error);
                });
            }

            prodavnicaService.getStatistikaProizvodiProdavnica(user.groupsid, 5)
            .then(res=>{
                setStatistikaProizvodi(res.data);
                console.log("statistika");
                console.log(res.data);
            })
            .catch(function (error)
            {
                console.log(error);
            });

            prodavnicaService.getStatistikaProdavciProdavnica(user.groupsid, 5)
            .then(res=>{
                setStatistikaProdavci(res.data);
                console.log("statistika");
                console.log(res.data);
            })
            .catch(function (error)
            {
                console.log(error);
            });
        }

        prodavnicaService.sveProdavnice()
        .then(res=>{
            var p=res.data;
            setProdavnice(p);
        })
        .catch(error => {
          console.log(error);
        });

        proizvodService.getProizvodi()
            .then(res =>{
                var p=res.data;
                setProizvodi(p);
                console.log(p);
            }).catch(function (error) {
                console.log(error);
            });
    
        return () => {};
      }, []);

      const handleChangeProdavnica=(ev)=>{
        setProdavnica(ev.target.value);
        okrugService.uzmiOkrug(ev.target.value.id)
        .then(res=>{
            setOkrug(res.data);
        })
        .catch(error => {
            console.log(error);
        });

        prodavnicaService.getStatistikaProizvodiProdavnica(ev.target.value.id, 5)
            .then(res=>{
                setStatistikaProizvodi(res.data);
                console.log("statistika");
                console.log(res.data);
            })
            .catch(function (error)
            {
                console.log(error);
            });

        prodavnicaService.getStatistikaProdavciProdavnica(ev.target.value.id, 5)
        .then(res=>{
            setStatistikaProdavci(res.data);
            console.log("statistika");
            console.log(res.data);
        })
        .catch(function (error)
        {
            console.log(error);
        });
      }

    const handleDeleteProdavnica = (ev) => {
        prodavnicaService.obrisiProdavnicu(prodavnica.id)
        .then(res=>{
            console.log(res.data);
            if(parseInt(currentUser.groupsid) === parseInt(prodavnica.id))
            {
                authService.logout();
                history.push('/');
            }
        })
        .catch(function (error)
        {
            console.log(error);
        });
    }

    const handleDeleteOkrug = (ev) => {
        okrugService.obrisiOkrug(okrug.id)
        .then(res=>{
            console.log(res.data);
            if(currentUser.unique_name !== "admin")
            {
                authService.logout();
                history.push('/');
            }
        })
        .catch(function (error)
        {
            console.log(error);
        });
    }

    const handleChangeKolicina=(ev, i)=>
    {
        let niz = kolicina;
        niz[i] = parseInt(ev.target.value);
        setKolicina(niz);

    }

    const handleDodajUKorpu =(ev,i)=>{
        if(Object.keys(prodavnica).length !== 0) {
            prodavnicaService.nadjiLinkProdavnicaPorizvod(proizvodi[i].id, prodavnica.id)
            .then(res=>{
                console.log(res.data);
                var r=res.data;
                if(r.success === false) {
                    prodavnicaService.nadjiProdavnicuSaOdredjenimProizvodom(proizvodi[i], prodavnica.id, okrug.id)
                    .then(res => {
                        console.log(res.data);
                        let mes = { open: true, type: "info", content: "Proizvod: " + proizvodi[i].naziv + " nije dostupan u ovoj prodavnici! " + res.data.message};
                        if(res.data.success === true){
                            setMessage({...mes, request: true});
                            setProizvodiZaPorucivanje([res.data.data]);
                        }
                        else
                            setMessage(mes);     
                    })
                    .catch(error => {
                        console.log(error);
                    });
                }
                else {
                    if(kolicina[i] <= r.data.kolicina) {
                        const tmpProizvod = {
                            kolicina: kolicina[i],
                            proizvod: proizvodi[i]
                        }
                        setProizvodiZaProdaju([...proizvodiZaProdaju, tmpProizvod]);
                        var niz=vidljiv.slice();
                        niz[i]=true;
                        setVidljiv(niz);

                        let cena = ukupnaCena + proizvodi[i].cena * kolicina[i];
                        setUkupnaCena(cena);
                    }
                    else {
                        prodavnicaService.nadjiProdavnicuSaOdredjenimProizvodom(proizvodi[i], prodavnica.id, okrug.id)
                        .then(res => {
                            console.log(res.data);
                            let mes = { open: true, type: "info", content: "Nema zahtevane količine! Trenutno dostupna količina: " + r.data.kolicina + "\n" + res.data.message };
                            if(res.data.success === true){
                                setMessage({...mes, request: true});
                                setProizvodiZaPorucivanje([res.data.data]);
                            }
                            else
                                setMessage(mes);
                        })
                        .catch(error => {
                            console.log(error);
                        });
                    }
                }
            })
            .catch(function (error){
                console.log(error);
            })
        }
        else
            setMessage({ open: true, type: "warning", content: "Morate prvo da izaberete prodavnicu!"});
    }

    const handleKupi = (ev) =>
    {   
        if(proizvodiZaProdaju.length > 0){
            prodavnicaService.izvrsiProdaju(prodavnica.id, okrug.id, currentUser.given_name, prodavnica.ime, proizvodiZaProdaju)
            .then(res => {
                console.log(res);
                let mes = { open: true, type: "success", content: res.data.message, reload: true };
                if(res.data.success === true){
                    setMessage({...mes, request: true});
                    setDialog({ ...dialog, prodaja: true });
                    setProizvodiZaPorucivanje([ ...res.data.data ]);
                }
                else
                    setMessage(mes);
            })
            .catch(error => {
                console.log(error);
            });
        }
        else
            setMessage({ open: true, type: "warning", content: "Nijedan proizvod nije izabran! Morate prvo da dodate neki proizvod za prodaju!"});
    }

    return (

        <div>
            <div className={classes.naslov}>
                <div>
                    {isAdminOrProdavac && (
                    <Button href={"/EditProdavnica/" + prodavnica.id} variant="outlined" style={{ margin: 8 }} color="primary" >
                        <EditIcon />Edit
                    </Button>
                    )}
                </div>
                <h1>{prodavnica.ime}</h1>
                <div>
                    {isAdmin && (
                        <Button variant="outlined" color="secondary" style={{ margin: 8 }} onClick={handleDeleteProdavnica}>
                            <DeleteIcon />Obriši
                        </Button>
                    )}
                </div>
            </div>
            {isAdmin && (
            <div>
                <FormControl variant="outlined" fullWidth className={classes.formControl}>
                    <InputLabel id="demo-simple-select-label">Izaberite prodavnicu</InputLabel>
                    <Select
                        labelId="demo-simple-select-label"
                        id="demo-simple-select"
                        //value={prodavnica}
                        //defaultValue={prodavnica}
                        onChange={(ev)=>handleChangeProdavnica(ev)}
                        label="Prodavnica u koju se dodaje"
                        style={{ marginRight: 15 }}
                    >
                        {prodavnice.map((prodavnica, ind)=>
                            <MenuItem key={ind} value={prodavnica}>{prodavnica.ime}</MenuItem>
                            )
                        }        
                    </Select>
                </FormControl>
            </div>
            )}
            <Grid item xs={12} md={6}>
                <Typography variant="h5" className={classes.title}>
                    Osnovne informacije
                </Typography>
                <div className={classes.demo}>
                    <List>
                        <ListItem>
                            <ListItemIcon>
                                <LocationCityIcon />
                            </ListItemIcon>
                            <ListItemText
                                primary={okrug.ime + " okrug"}
                            />
                            {isAdmin && (
                                <>
                                    <ListItemSecondaryAction>
                                        <Button variant="outlined" color="secondary" onClick={handleDeleteOkrug}>
                                            <DeleteIcon />Obriši Okrug
                                        </Button>
                                    </ListItemSecondaryAction>
                                </>
                            )}
                        </ListItem>
                        <ListItem>
                            <ListItemIcon>
                                <HomeWorkIcon />
                            </ListItemIcon>
                            <ListItemText
                                primary={prodavnica.adresa}
                            />
                        </ListItem>
                    </List>
                    </div>
                </Grid>
                
                <div className={classes.naslov}>
                    <StatisitkaLista naslov={"Najprodavaniji proizvodi"} lista={statistikaProizvodi}/>

                    <div className={classes.form}>
                        {proizvodi.map((proizvod, i)=>
                        <div key={i} >
                            <h4>{proizvod.naziv} - {proizvod.cena} din</h4>
                            <TextField  placeholder="Kolicina" disabled={vidljiv[i]} onChange={(ev)=>handleChangeKolicina(ev, i)}></TextField>
                            <Button color="primary" style={{ marginLeft: 10}} onClick={(ev)=>handleDodajUKorpu(ev,i)} disabled={vidljiv[i]}>Dodaj u korpu</Button>
                            {vidljiv[i] ? <CheckCircleIcon className={classes.check} /> : null}
                        </div>)}
                        <div style={{ margin: 10, marginTop: 35 }}>
                        <TextField label="Ukupna cena" value={ukupnaCena} disabled style={{ marginTop: -10, marginInline: 10}}></TextField>
                        <Button variant="outlined" disabled={!isUsersProdavnica} onClick={handleKupi}>Izvrši prodaju</Button>
                        </div>
                    </div>

                    <StatisitkaLista naslov={"Najbolji prodavci"} lista={statistikaProdavci}/>
                </div>

                <div className={classes.root}>
                    <Snackbar open={message.open} autoHideDuration={10000} onClose={handleClose} >
                        <Alert 
                            onClose={handleClose} 
                            severity={message.type} 
                            action={
                            <>
                                {message.request && (
                                    <Button color="inherit" size="small" onClick={handleNaruci}>
                                        Naruči
                                    </Button>
                                )}
                                <Button color="inherit" size="small" onClick={handleClose}>
                                    <CloseIcon />
                                </Button>
                            </>
                            }
                        >
                            {message.content}
                        </Alert>
                    </Snackbar>
                </div>

                <div>
                    <Dialog open={dialog.open} onClose={handleClose} aria-labelledby="form-dialog-title">
                        <DialogTitle id="form-dialog-title">Narudžbenica</DialogTitle>
                        <DialogContent>
                        <DialogContentText>
                            {dialog.content}
                        </DialogContentText>
                        <TextField
                            autoFocus
                            margin="dense"
                            id="name"
                            label="Količina"
                            type="number"
                            fullWidth
                            onChange={handleChangeDialogKolicina}
                        />
                        </DialogContent>
                        <DialogActions>
                        <Button color="primary" onClick={handlePosaljiZahtev}>
                            Pošalji zahtev
                            <SendIcon style={{ margin: 10}}/>
                        </Button>
                        <Button color="secondary" onClick={handleCloseDialog}>
                            Poništi
                        </Button>
                        </DialogActions>
                    </Dialog>
                </div>

        </div>
    )
}
