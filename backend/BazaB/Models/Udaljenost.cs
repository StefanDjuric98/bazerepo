namespace BazaB.Models
{

    public class Udaljenost
    {
        public int Razdaljina { get; set; }
        public Okrug OdOkrug { get; set; }
        public Okrug DoOkrug { get; set; }
    }
}