import React, { useState, useEffect } from 'react'
import{useParams} from "react-router-dom";

import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';

import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';

//service
import proizvodService from '../services/proizvod.service'
import prodavnicaService from '../services/prodavnica.service'
import authService from '../services/auth.service';
import StatisitkaLista from '../components/StatistikaLista';

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const useStyles = makeStyles((theme) => ({
    input: {
      width: "100%",
      maxWidth: 50,
      marginRight: "10px"
    },
    centar:{
      display: "flex",
      flexDirection: "column",
      alignItems: "center",
      width: "100%",
      margin: "auto",
      padding: "10px"
    },
    proizvod: {
      display: "flex",
      flexWrap: "nowrap",
      margin: 15,
    },
    formControl: {
      width: "100%",
      margin: theme.spacing(1),
      minWidth: 600,
    },
    form: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-around',
      flexWrap: 'nowrap',
  },
  }));

export default function DodajProizvode() {
    const { idKorisnika } = useParams();
    const classes = useStyles();
    const [proizvodi,setProizvodi]=useState([]);
    const [currentUser, setCurrentUser] = useState(undefined);
    const [prodavnice, setProdavnice]=useState([]);
    const [izabranaProdavnica, setIzabranaPodavnica]=useState({});
    const [kolicina,setKolicina]=useState([]);
    const [minKolicina, setMinKolicina]=useState([]);
    const [message, setMessage] = useState({open: false, type: "succes", content: ""});
    const [statistikaProizvodi, setStatistikaProizvodi] = useState([]);

    const isAdmin = currentUser && currentUser.role === "Admin";
  
    const handleClose = (event, reason) => {
      if (reason === 'clickaway') {
        return;
      }
  
      setMessage({ ...message, open: false });
    };

    useEffect(() => {

      proizvodService.getProizvodi()
        .then(res =>{
            var p=res.data;
            setProizvodi(p);
            console.log(p);
        }).catch(function (error) {
            console.log(error);
        });

      prodavnicaService.sveProdavnice()
        .then(res=>{
            var p=res.data;
            setProdavnice(p);
        }).catch(function (error) {
          console.log(error);
        });


      const user = authService.getCurrentUser();

      if (user) {
          setCurrentUser(user);

          prodavnicaService.getStatistikaProizvodiProdavnica(user.groupsid, 5)
          .then(res=>{
              setStatistikaProizvodi(res.data);
          })
          .catch(function (error)
          {
              console.log(error);
          });
      };

      
  
      return () => {};
    }, []);

    const handleDeleteProizvod = (ev, id) => {
      proizvodService.obrisiProizvod(id)
      .then(res=>{
        console.log(res);
        window.location.reload();
      }).catch(error => {
        console.log(error);
      });
    }

    const handleChangeProdavnica=(ev)=>{
      setIzabranaPodavnica(ev.target.value);

      prodavnicaService.getStatistikaProizvodiProdavnica(ev.target.value.id, 5)
        .then(res=>{
            setStatistikaProizvodi(res.data);
            console.log("statistika");
            console.log(res.data);
        })
        .catch(function (error)
        {
            console.log(error);
        });
  }
    const handleChangeKolicina=(ev,i)=>{
      var niz=kolicina.slice();
      niz[i]=ev.target.value;
      setKolicina(niz);
      // console.log(niz);
    }

    const handleChangeMinKolicina=(ev,i)=>{
      var niz=minKolicina.slice();
      niz[i]=ev.target.value;
      setMinKolicina(niz);
    }

    const handleDodajUprodavnicu=(ev,id,i)=>{
      if(isAdmin && Object.keys(izabranaProdavnica).length === 0){
        setMessage({ open: true, type: "warning", content: "Morate da izaberete prodavnicu pre dodavanja proizvoda!"});
        return;
      }
      
      var proizvodKonkretan=proizvodi[i];
      var k=kolicina[i];
      var minKol= minKolicina[i];
      console.log("proizvod[i]"+proizvodi[i]);
      console.log(i);
      console.log("Kolicina :"+k);
      console.log("Minimalna:" + minKol);
      if(proizvodi[i].kolicina>=k)
      {
        if(parseInt(k)>parseInt(minKol))
        {
          let data={
            kolicina:parseInt(k),
            minKolicina:parseInt(minKol),
            proizvod:proizvodKonkretan,
            prodavnica: izabranaProdavnica,
    
          }
          prodavnicaService.kreirajLinkProizvod(data)
          .then(res=>{
            console.log(res);
            console.log(res.data);
            proizvodKonkretan.kolicina = proizvodKonkretan.kolicina - parseInt(k);
            let nizProizvodi = proizvodi;
            nizProizvodi[i] = proizvodKonkretan;
            setProizvodi(nizProizvodi);
            setMessage({ open: true, type: "success", content: "Uspesno ste dodali proizvod: " + proizvodKonkretan.naziv});
        })
        .catch(function (error) {
          console.log(error);
        })
        }
        else{
          setMessage({ open: true, type: "error", content: "Minimalna kolicina: " + minKol+ " je veca od kolicine: " + k});
        }

      }
      else{
        setMessage({ open: true, type: "error", content: "Kolicina koju zelite da uneste (" + k + ") je veca od dostupne (" + proizvodi[i].kolicina + ")"});
      }

    }
 

    return (
        <div className={classes.centar}>
            <h1>Dodaj proizvode u prodavnicu</h1>
            {isAdmin && (
            <div>
                <FormControl variant="outlined" fullWidth className={classes.formControl}>
                    <InputLabel id="demo-simple-select-label">Prodavnica u koju se dodaje</InputLabel>
                    <Select
                        labelId="demo-simple-select-label"
                        id="demo-simple-select"
                        //value={prodavnica}
                        defaultValue=""
                        onChange={(ev)=>handleChangeProdavnica(ev)}
                        label="Prodavnica u koju se dodaje"
                    >
                        {prodavnice.map((prodavnica, ind)=>
                            <MenuItem key={ind} value={prodavnica}>{prodavnica.ime}</MenuItem>
                            )
                        }        
                    </Select>
                </FormControl>
            </div>
            )}
          <div className={classes.form}>
            <div>
              <InputLabel id="demo-simple-select-label" style={{ margin: 8 }}><strong>Dostupni proizvodi:</strong></InputLabel>
              {proizvodi.map((proizvod,i) =>
              <div key={proizvod.id} id={proizvod.id} className={classes.proizvod}> 
                <InputLabel id="demo-simple-select-label" style={{ margin: 8 }}>{proizvod.naziv}</InputLabel>
                <InputLabel id="demo-simple-select-label" style={{ margin: 8 }}>Dostupno je: {proizvod.kolicina}</InputLabel>
                <TextField id="standard-basic" placeholder="Zahtevana Kolicina" type="number" className="kolicina" style={{ margin: 8 }} onChange={(ev)=>handleChangeKolicina(ev,i)}/>
                {/* <input type="number" id={proizvod.id} size="20" className="kolicina" /> */}
                <TextField id="standard-basic" placeholder="Minimalna kolicina" type="number" className="minKolicina" style={{ margin: 8 }} onChange={(ev)=>handleChangeMinKolicina(ev,i)}/>
                {/* <input type="number"  size="20" className="minKolicina" /> */}
                <Button variant="outlined" onClick={(ev)=> handleDodajUprodavnicu(ev,proizvod.id,i)} >
                          Dodaj
                </Button>
                {isAdmin && (
                  <>
                    <Button href={"/EditProizvod/" + proizvod.id} variant="outlined" style={{ margin: 8 }} color="primary" >
                      <EditIcon />Edit
                    </Button>
                    <Button variant="outlined" color="secondary" style={{ margin: 8 }} onClick={(ev) => handleDeleteProizvod(ev, proizvod.id)}>
                        <DeleteIcon />Obriši
                    </Button>
                  </>
                  )}
                
              </div>)
              }
            </div>

            <StatisitkaLista naslov={"Najprodavaniji proizvodi"} lista={statistikaProizvodi}/>
          </div>


          <div className={classes.root}>
                    <Snackbar open={message.open} autoHideDuration={4000} onClose={handleClose}>
                        <Alert onClose={handleClose} severity={message.type}>
                            {message.content}
                        </Alert>
                    </Snackbar>
          </div>
            
        </div>
    )
}
