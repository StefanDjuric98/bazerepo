
using System.Text.Json;

namespace BazaB.Models
{

    public class ProdavacDTO
    {
        public string Ime { get; set; }
        public string Prodavnica { get; set; }

        public string ToJsonString()
        {
            return JsonSerializer.Serialize<ProdavacDTO>(this);
        }
    }
}