import React, { useState, useEffect } from 'react'
import { useHistory } from "react-router-dom";

import { makeStyles } from "@material-ui/core/styles";
import TextField from '@material-ui/core/TextField';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";
import Button from '@material-ui/core/Button';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import okrugService from '../services/okrug.service';

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const useStyles = makeStyles((theme) => ({
    root: {
      width: '100%',
      maxWidth: 360,
      backgroundColor: theme.palette.background.paper,
    },
    input: {
      width: "100%",
      maxWidth: 50,
      marginRight: "10px"
    },
    formControl: {
      margin: theme.spacing(1),
      minWidth: 120,
    },
    selectEmpty: {
      marginTop: theme.spacing(2),
    },
    form: {
      width: "100%", 
      marginTop: theme.spacing(1),
      display: "flex",
      flexDirection: "column",
      alignItems: "center",
  },
  }));

export default function KreirajOkrug() {

    const classes = useStyles();
    const [okruzi, setOkruzi] = useState([]);
    const [okrug, setOkrug]=useState({});
    const [veze, setVeze] = useState([
      {
          razdaljina: 0,
          doOkrug: {}
      }]);
    const [brojVeza, setBrojVeza] = useState([1]);
    const [message, setMessage] = React.useState({open: false, type: "succes", content: ""});

    const handleClose = (event, reason) => {
      if (reason === 'clickaway') {
        return;
      }
  
      setMessage({ ...message, open: false });
      if(message.type === "success")
          window.location.reload();
    };

    useEffect(() => {
        okrugService.uzmiSveOkruge()
            .then(res => {
                var o  =res.data
                setOkruzi(o);
                console.log(o);
            }).catch(error => {
                console.log(error);
            });
    
        return () => {};
      }, []);

    const handleChangeIme=(ev)=>{
        setOkrug({ ...okrug, ime: ev.target.value });
    }
    const handleChangeRazdaljina=(ev, i)=>{
      let niz = veze.slice();
      niz[i].razdaljina = parseInt(ev.target.value);
      setVeze(niz);
    }

    const handleChangeSelect=(ev, i)=>{
      let niz = veze.slice();
      niz[i].doOkrug = ev.target.value;
      setVeze(niz);
      console.log(veze);
  }

    const handleNovaVeza=(ev)=>{
      const br = brojVeza[brojVeza.length - 1] + 1;
      setBrojVeza([ ...brojVeza, br]);
      console.log(brojVeza);
      const novaVeza = 
      {
        razdaljina: 0,
        doOkrug: {}
      };
      setVeze([ ...veze, novaVeza]);
      console.log("Dodata nova veza:");
      console.log(veze);
    }

    const handleSubmit=(ev)=>{
      ev.preventDefault();
      console.log(okrug);
      console.log(veze);

      const okrugDTO = {
        okrug: okrug,
        veze: veze
    };
        
      okrugService.kreirajOkrug(okrugDTO)
      .then(res => {
        console.log(res);
        console.log(res.data);
        setMessage({ open: true, type: "success", content: [res.data]});
      })
      .catch(err => {
        console.log(err);
        setMessage({ open: true, type: "error", content: [err.response.data]});
      });
    }
    
    return (
        <div className={classes.form}>
            <h1>Kreiraj Okrug</h1>
            <form onSubmit={handleSubmit} >
                <TextField label="Ime okruga" variant="outlined" fullWidth style={{ margin: 8 }} onChange={handleChangeIme}/>
                <div>
                    <InputLabel style={{ margin: 15 }}><strong>Okruzi sa kojima je povezan</strong></InputLabel>
                    {brojVeza.map((veza, i) => 
                      <div>
                        <FormControl variant="outlined" className={classes.formControl}>
                            <InputLabel id="demo-simple-select-outlined-label">Okrug</InputLabel>
                            <Select
                              labelId="demo-simple-select-outlined-label"
                              id="demo-simple-select-outlined"
                              value={veze[i].doOkrug}
                              onChange={(ev) => handleChangeSelect(ev, i)}
                              label="Okrug"
                            >
                              {okruzi.map((okrug, ind) => 
                                <MenuItem key={ind} value={okrug}>{okrug.ime}</MenuItem>
                                )
                              }
                            </Select>
                        </FormControl>
                        <TextField id="outlined-basic" variant="outlined" style={{ margin: 8 }} placeholder="Razdaljina" type="number" onChange={(ev) => handleChangeRazdaljina(ev, i) } />
                    </div>
                    )}
                    
                    <div><Button color="primary" onClick={handleNovaVeza}>+ Nova veza</Button></div>
                </div>
                <Button variant="contained" color="primary" style={{ margin: 8 }} fullWidth type="submit">Kreiraj Okrug</Button>
            </form>

            <div className={classes.root}>
                    <Snackbar open={message.open} autoHideDuration={4000} onClose={handleClose}>
                        <Alert onClose={handleClose} severity={message.type}>
                            {message.content}
                        </Alert>
                    </Snackbar>
            </div>
        </div>
    )
}
