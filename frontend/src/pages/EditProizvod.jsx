import React, { useState, useEffect } from 'react'
import { useHistory } from "react-router-dom";

import { makeStyles } from "@material-ui/core/styles";
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';

//service
import proizvodService from '../services/proizvod.service'

function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const useStyles = makeStyles((theme) => ({
    form: {
      width: "100%", 
      marginTop: theme.spacing(1),
      display: "flex",
      flexDirection: "column",
      alignItems: "center",
  },
  }));

export default function EditProizvod(props) {

    let history = useHistory();
    const classes = useStyles();
    const [naziv, setNaziv]=useState('');
    const [vrsta, setVrsta]=useState('');
    const [cena, setCena]=useState('');
    const [kolicina, setKolicina]=useState('');
    const [message, setMessage] = React.useState({open: false, type: "succes", content: ""});

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
          return;
        }
    
        setMessage({ ...message, open: false });
        if(message.type === "success")
            history.push("/DodajProizvod");
    };

    useEffect(() => {
        console.log(props.id);
        proizvodService.uzmiProizvod(props.id)
        .then(res =>{
            var proizvod = res.data
            setNaziv(proizvod.naziv);
            setVrsta(proizvod.vrsta);
            setCena(proizvod.cena);
            setKolicina(proizvod.kolicina);
        }).catch(error => {
            console.log(error);
        });
 
        return () => {
        }
    },[props.id])

    const handleChangeNaziv=(ev)=>{
        setNaziv(ev.target.value);
    }
    const handleChangeVrsta=(ev)=>{
        setVrsta(ev.target.value);
    }
    const handleChangeCena=(ev)=>{
        setCena(parseInt(ev.target.value));
       
    }
    const handleChangekolicina=(ev)=>{
        setKolicina(parseInt(ev.target.value));
    }
    const handleSubmit=(ev)=>{
        ev.preventDefault();
   
        const proizvod={
            id: props.id,
            naziv: naziv,
            vrsta: vrsta,
            cena: cena,
            kolicina: kolicina,
        }

        proizvodService.izmeniProizvod(proizvod)
        .then(res=>{
            console.log(res);
            console.log(res.data);
            setMessage({ open: true, type: "success", content: [res.data]});
        })
        .catch(function (error) {
            console.log(error);
            setMessage({ open: true, type: "error", content: [error.response.data]});
        });
    }

    return (
        <div className={classes.form}>
            
            <form onSubmit={handleSubmit}>
                <h1 style={{ margin: 20 }}>Kreiraj novi proizvod</h1>
               <div><TextField id="standard-basic" variant="outlined" fullWidth style={{ margin: 8 }} label="Naziv" value={naziv} onChange={handleChangeNaziv}/></div> 
               <div><TextField id="standard-basic" variant="outlined" fullWidth style={{ margin: 8 }} label="Vrsta" value={vrsta} onChange={handleChangeVrsta}/></div>
               <div><TextField id="standard-basic" variant="outlined" fullWidth style={{ margin: 8 }} label="Cena" value={cena} type="number" onChange={handleChangeCena}/></div>
               <div><TextField id="standard-basic" variant="outlined" fullWidth style={{ margin: 8 }} label="Kolicina" value={kolicina} type="number" onChange={handleChangekolicina}/></div>
               <div>
                   <Button variant="contained" color="primary" fullWidth type="submit" style={{ margin: 8 }}>
                        Izmeni proizvod
                    </Button>
                </div>
            </form>

            <div className={classes.root}>
                    <Snackbar open={message.open} autoHideDuration={3000} onClose={handleClose}>
                        <Alert onClose={handleClose} severity={message.type}>
                            {message.content}
                        </Alert>
                    </Snackbar>
            </div>
        </div>
    )
}
