using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Neo4j.Driver;
using System.Text;
using BazaB.Models;
using System.Net.Http;
using Microsoft.AspNetCore.Authorization;

[Route("api/[controller]")]
[ApiController]
public class OkrugController : ControllerBase
{
    private readonly IDriver _driver;
    private readonly RedisService _redisService;

    public OkrugController(IDriver driver, RedisService redisService)
    {
        _driver = driver;
        _redisService = redisService;
    }

    [Authorize(Roles = "Admin")]
    [HttpPost]
    [Route("KreirajOkrug")]
    public async Task<IActionResult> CreateOkrug(OkrugDTO okrugDTO)
    {
        var session = this._driver.AsyncSession();
        try
        {
            bool okrugExists = await session.ReadTransactionAsync(async tx =>
            {
                var result = await tx.RunAsync("MATCH (o:Okrug { ime: '" + okrugDTO.okrug.Ime + "' }) RETURN count(*) as count");
            
                return await result.SingleAsync(res =>
                {
                    return res["count"].As<int>() > 0;
                });
            });

            if(okrugExists)
                return BadRequest("Okrug: " + okrugDTO.okrug.Ime + " already exists in the database!");
            
            int id = NextIdHelper.GetId(_driver).Result;
            string statementText = "CREATE (o:Okrug {id: $id , ime: $ime})";
            var statementParameters = new Dictionary<string, Object>
            {
                {"id", id},
                {"ime", okrugDTO.okrug.Ime }
            };
            await session.WriteTransactionAsync(tx => tx.RunAsync(statementText, statementParameters));

            statementText = "MATCH (o1:Okrug {id: $idOd}), (o2:Okrug { id: $idDo})" +
                            "CREATE (o1)-[r:POVEZAN { razdaljina: $razdaljina }]->(o2)";
            
            foreach(Udaljenost veza in okrugDTO.veze)
            {
                statementParameters = new Dictionary<string, Object>
                {
                    {"idOd", id},
                    {"idDo", veza.DoOkrug.ID},
                    {"razdaljina", veza.Razdaljina}
                };
                await session.WriteTransactionAsync(tx => tx.RunAsync(statementText, statementParameters));
            }
 
            return Ok("Node Okrug id: " + id + " has been created in the database");
        }
        finally
        {
            await session.CloseAsync();
        }
    }

    [Authorize(Roles = "Admin,Prodavac")]
    [HttpGet]
    [Route("SviOkruzi")]
    public async Task<List<Okrug>> GetOkruzi()
    {
        var session = _driver.AsyncSession();
        try
        {
            var list = new List<Okrug>();
            return await session.ReadTransactionAsync(async tx =>
            {
                var result = await tx.RunAsync("MATCH (o:Okrug) RETURN o ");

                await result.ForEachAsync(res =>
                {
                    var node = res["o"].As<INode>();
                    list.Add(new Okrug()
                    {
                        ID = node.Properties["id"].As<int>(),
                        Ime = node.Properties["ime"].As<string>(),
                    });
                });
                return list;
            });
        }
        finally
        {
            await session.CloseAsync();
        }
    }

    [Authorize(Roles = "Admin")]
    [HttpDelete]
    [Route("ObrisiOkrug/{id}")]
    public async Task<IActionResult> DeleteOkrug(int id)
    {
        var session = _driver.AsyncSession();
        try
        {
            List<Prodavnica> prodavnice = new List<Prodavnica>();
            List<List<ProdavacDTO>> prodavci = new List<List<ProdavacDTO>>();
            await session.ReadTransactionAsync(async tx =>
            {
                var result = await tx.RunAsync("MATCH (o:Okrug { id: " + id + " }) " +
                                                "OPTIONAL MATCH (o)<-[:PRIPADA]-(p:Prodavnica) OPTIONAL MATCH (p)<-[:RADI_U]-(prod:Prodavac) " +
                                                "RETURN p.id as pid, p.ime as pime, prod.name as ime, prod.prezime as prezime");
                return await result.ForEachAsync(res =>
                {
                    if(res["pid"] == null)
                        return;

                    if((prodavnice.Count-1) < 0 || prodavnice[prodavnice.Count - 1].ID != res["pid"].As<int>())
                    {
                        prodavnice.Add(new Prodavnica()
                        {
                            ID = res["pid"].As<int>(),
                            Ime = res["pime"].As<string>()
                        });
                        prodavci.Add(new List<ProdavacDTO>());
                    }
                    
                    if(res["ime"] != null)
                    {
                        prodavci[prodavnice.Count - 1].Add(new ProdavacDTO()
                        {
                            Ime = res["ime"].As<string>() + " " + res["prezime"].As<string>(),
                            Prodavnica = res["pime"].As<string>()
                        });
                    }
                    
                });
            });

            string query = "MATCH (o:Okrug { id:" + id + " })" +
                        " OPTIONAL MATCH (o)<-[:PRIPADA]-(p:Prodavnica)" +
                        " OPTIONAL MATCH (p)<-[:RADI_U]-(prod:Prodavac)" +
                        " DETACH DELETE prod, p, o";

            var result = await session.WriteTransactionAsync(tx => tx.RunAsync(query));
            
            await _redisService.DeleteOkrug(prodavnice, prodavci);
            
            return Ok("Obrisan okrug id: " + id);
        }
        finally
        {
            await session.CloseAsync();
        }
    }

    [HttpGet]
    [Route("NadjiOkrug/{idProdavnica}")]
    public async Task<Okrug> NadjiOkrug( int idProdavnica)
    {
        var session= _driver.AsyncSession();
        try{
            return await session.ReadTransactionAsync(async tx=>
            {
                Okrug okrug;
                var result= await tx.RunAsync("MATCH (a:Okrug)-[r:PRIPADA]-(p:Prodavnica{id:"+idProdavnica+"}) RETURN a");
                
                okrug = await result.SingleAsync(res=>
                {
                    var node=res["a"].As<INode>();
                    return new Okrug()
                    {
                        ID = node.Properties["id"].As<int>(),
                        Ime = node.Properties["ime"].As<string>(),
                    };
                });
                return okrug;
            });
        }
        finally
        {
            await session.CloseAsync();
        }
    }
}